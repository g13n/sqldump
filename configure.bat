@echo off
rem ---------------------------------------------------------------------------
rem Configuration script for SqlDump
rem
rem Copyright (C) 2001-2002 Gopalarathnam V. gopal@developercentral.org
rem
rem This file is a part of SqlDump.
rem
rem This program is free software; you can redistribute it and/or
rem modify it under the terms of the GNU General Public License
rem as published by the Free Software Foundation; either version 2
rem of the License, or (at your option) any later version.
rem
rem This program is distributed in the hope that it will be useful,
rem but WITHOUT ANY WARRANTY; without even the implied warranty of
rem MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
rem GNU General Public License for more details.
rem
rem You should have received a copy of the GNU General Public License
rem along with SqlDump; if not, write to the Free Software
rem Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
rem ---------------------------------------------------------------------------
rem
rem YOU'LL NEED THE FOLLOWING TO BUILD SQLDUMP:
rem
rem    * Microsoft Windows 9x/Me/2000/XP
rem    * MSVC 5.x or later, or
rem      BCC 5.5 or later, or
rem      gcc 2.95 or later and the MinGW and W32 headers and libraries

rem See if the environment is large enough. A test for 43! bytes will reveal
rem the status.
rem
set FOO=0123456789012345678901234567890123456789012
if not "%FOO%" == "0123456789012345678901234567890123456789012" goto smallenv
set FOO=

:start
rem  Set default settings
rem
set PREFIX=
set COMPILER=
set CC=

rem  Handle arguments
rem
:again
if "%1" == "--help" goto usage
if "%1" == "--prefix" goto setprefix
if "%1" == "--with-msvc" goto withmsvc
if "%1" == "--with-bcc" goto withbcc
if "%1" == "--with-gcc" goto withgcc
if "%1" == "--with-debug" goto setdebug
if "%1" == "--no-opt" goto noopt
if "%1" == "" goto check
:usage
echo Usage: configure [options]
echo Options:
echo.  --help           print this help screen
echo.  --prefix PREFIX  install SqlDump in directory PREFIX
echo.  --with-msvc      use Microsoft Visual C++ to compile SqlDump
echo.  --with-bcc       use Borland C++ to compile SqlDump
echo.  --with-gcc       use GNU C Compiler to compile SqlDump
echo.  --with-debug     include debugging info in the executables
echo.  --no-opt         disable optimization
goto end

:setprefix
rem  Set the installation directory
shift
set PREFIX=%1
shift
goto again

:withmsvc
rem  Use Microsoft Visual C++ to compile SqlDump
set CC=cl
set COMPILER=msvc
shift
goto again

:withbcc
rem  Use Borland C++ to compile SqlDump
set CC=bcc32
set COMPILER=bcc
shift
goto again

:withgcc
rem  Use GNU C Compiler compile SqlDump
set CC=gcc
set COMPILER=gcc
shift
goto again

:withdebug
rem  Include debugging info in the executables
set DEBUG=Y
shift
goto again

:noopt
rem  Disable optimization
set NOOPT=Y
shift
goto again

rem  Attempt to auto-detect compiler if not specified
:check
if "%COMPILER%" == "msvc" goto genmakefiles
if "%COMPILER%" == "bcc" goto genmakefiles
if "%COMPILER%" == "gcc" goto genmakefiles

echo Checking whether 'cl' is available...
echo main(){} >junk.c
cl /nologo /c junk.c
if exist junk.obj goto clOK

echo Checking whether 'bcc32' is available...
bcc32 -c junk.c
if exist junk.obj goto bccOK

echo Checking whether 'gcc' is available...
gcc -c junk.c
if not exist junk.o goto nocompiler
del junk.o
echo Using 'GNU C Compiler'
set CC=gcc
set COMPILER=gcc
del junk.o
del junk.c
goto genmakefiles

:nocompiler
echo.
echo Configure failed.
echo To configure SqlDump, you need to have MSVC 5.x or later,
echo or Borland C++ 5.5 or later,
echo or gcc 2.95 or later and the MinGW and W32 headers and libraries.
del junk.c
goto end

:clOK
echo Using 'Microsoft Visual C++'
set CC=cl
set COMPILER=msvc
del junk.obj
del junk.c
goto genmakefiles

:bccOK
echo Using 'Borland C++'
set CC=bcc32
set COMPILER=bcc
del junk.obj
del junk.c
goto genmakefiles

:genmakefiles
echo Generating makefiles...
if "%COMPILER%" == "msvc" set MAKECMD=nmake
if "%COMPILER%" == "bcc" set MAKECMD=make
if "%COMPILER%" == "gcc" set MAKECMD=gmake
rem  Pass on chosen settings to makefiles
rem  NB. Be very careful not to have any spaces before redirection
rem  symbols except when preceded by numbers.
rem
echo # Start of settings from configure.bat >config.settings
echo COMPILER=%COMPILER%>>config.settings
echo MAKECMD=%MAKECMD%>>config.settings
if "%DEBUG%" == "Y" echo DEBUG=1 >>config.settings
if "%NOOPT%" == "Y" echo NOOPT=1 >>config.settings
if not "%PREFIX%" == "" echo INSTALL_DIR=%PREFIX%>>config.settings
echo # End of settings from configure.bat>>config.settings
echo. >>config.settings

if exist src\Makefile del src\Makefile
copy /b config.settings+%MAKECMD%.defs+src\Makefile.%COMPILER%.in src\Makefile

copy /b config.settings+Makefile.in Makefile
del config.settings

echo Generating config.h...
echo /* This file is automatically generated from config.in. */ >config.h
echo. >>config.h
echo #ifndef _CONFIG_H >>config.h
echo #define _CONFIG_H >>config.h
echo. >>config.h
type config.in >>config.h
set configcmd=%0
:nextarg
if "%1" == "" goto noargs
set configcmd=%configcmd% %1
shift
goto nextarg
:noargs
echo. >>config.h
echo /* How SqlDump was configured? */ >>config.h
echo #define CONFIG_COMMAND "%configcmd%" >>config.h
echo. >>config.h
echo #endif /* whole file */ >>config.h

echo.
echo SqlDump successfully configured.
echo Run '%MAKECMD%' to build, then run '%MAKECMD% install' to install.
goto end

:smallenv
echo Your environment size is too small. Please enlarge it and rerun configure.
echo For example, type "command /e:1024" to have 1024 bytes available.
set FOO=

:end
set PREFIX=
set CC=
set COMPILER=
set DEBUG=
set NOOPT=
set MAKECMD=
