# -*- mode: Makefile -*-
#
# Makefile definition file for building SqlDump using Microsoft Visual C++
#
# Copyright (C) 2001-2002 Gopalarathnam V. <gopal@developercentral.org>.
#
# This file is a part of SqlDump.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SqlDump; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#

# INSTALL_DIR is the directory into which SqlDump will be installed.
#
!ifdef INSTALL_DIR
INSTALL_DIR	= $(INSTALL_DIR)\SqlDump
!endif

!ifndef INSTALL_DIR
!message WARNING: INSTALL_DIR not defined, defaulting to C:\SqlDump
INSTALL_DIR	= C:\SqlDump
!endif

# Check that the INCLUDE and LIB environment variables are set.
#
!ifndef INCLUDE
!error FATAL: The INCLUDE environment variable needs to be set
!endif
!ifndef LIB
!error FATAL: The LIB environment variable needs to be set
!endif

# Programs definitions for use by make
AR		= lib
AR_OUT		= /OUT:
CC		= cl
CC_OUT		= /Fo
LINK		= link
LINK_OUT	= /OUT:
RC		= rc
RC_FLAGS	= /r /x
RC_OUT		= /fo
RC_INCLUDE	= /i

BASELIBS	= 
O		= .obj
A		= .lib
EXE		= .exe

!ifdef DEBUG
DEBUG_CFLAGS	= /Zi
DEBUG_LINK	= /DEBUG
!endif

!ifndef NOOPT
DEBUG_CFLAGS	= /O2
DEBUG_LINK	= /RELEASE
!endif

CFLAGS		= /nologo /I.. /I. /I"$(INCLUDE)" $(DEBUG_CFLAGS) \
		  /D_WIN32_IE=0x0400 /GA /c
LINK_FLAGS	= /nologo /LIBPATH:"$(LIB)" $(DEBUG_LINK)

COMPILER_TEMP_FILES = *.aps

.SUFFIXES: .c $(O) .rc .res .dll $(A)

.c$(O):
	$(CC) $(CFLAGS) $(CC_OUT)$@ /Tp $<

.rc.res:
	$(RC) $(RC_INCLUDE)"$(INCLUDE)" $(RC_FLAGS) $(RC_OUT)$@ $<
