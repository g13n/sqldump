/*
	Copyright (C) 2001-2002 Gopalarathnam V. <gopal@developercentral.org>.
	
	This file is a part of SqlDump.
	
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with SqlDump; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "mssql.h"				/* definitions from the libmssql library */
#include "sqlutil.h"			/* sql utility functions */

#ifndef _SQLWRAP_H
#define _SQLWRAP_H

/* index into `sp_helpuser' */
#define SHOW_USERNAME 0
#define SHOW_GROUP    1
#define SHOW_LOGIN    2
#define SHOW_DATABASE 3

/* index into `sp_tables' */
#define SHOW_TABLENAME 2

/* index into `sp_columns' */
#define SHOW_FIELDNAME 3
#define DATA_TYPE      4
#define SHOW_TYPE      5
#define SHOW_PRECISION 6
#define SHOW_LENGTH    7
#define SHOW_SCALE     8
#define SHOW_NULL      10
#define SHOW_DEFAULT   12

#define TYPE_NUMERIC 2			/* to identify numeric columns */

/* index into `sp_helpindex' */
#define SHOW_INDEXNAME 0
#define SHOW_INDEXDESC 1
#define SHOW_INDEXKEYS 2

/* index into `sp_fkeys' */
#define SHOW_PKTABLE   2
#define SHOW_PKCOLNAME 3
#define SHOW_FKTABLE   6
#define SHOW_FKCOLNAME 7
#define SHOW_FKNAME    11

/* index into `sp_stored_procedures' */
#define SHOW_PROCNAME  2

/* index into `views' */
#define SHOW_VIEWNAME  0

/* index into `systypes' */
#define SHOW_UDTNAME   0
#define SHOW_UDTTYPE   1
#define SHOW_UDTLENGTH 2
#define SHOW_UDTNULL   3
#define SHOW_UDTPREC   4
#define SHOW_UDTSCALE  5

extern bool adddrop;			/* add drop table statements? */
extern bool dumpdefn;			/* dump table definitions? */
extern bool dumpdata;			/* dump table data? */
extern bool dumpuser;			/* dump database users? */
extern bool dumpprocs;			/* dump stored procedures? */
extern bool dumpviews;			/* dump views? */
extern bool dumpudts;			/* dump user-defined types? */
extern bool listserv;			/* list registered sql servers? */
extern char *where;				/* query condition to be matched */

extern bool allok;				/* 1 if everything was successful! */
extern FILE* lf;				/* log file pointer */

extern void write_header(const char*, MSSQL*, FILE*);
extern MSSQL* serverconnect(char*, char*, char*, char*);
extern void serverdisconnect(MSSQL*);
extern ulong space_used(MSSQL*, const char* table);
extern char* get_table_name(MSSQL*);
extern int get_table_structure(MSSQL*, const char*, FILE*);
extern unsigned long dump_table(MSSQL*, const char*, uint, FILE*);
extern int dump_users(MSSQL*, FILE*);
extern char* get_view_name(MSSQL*);
extern int dump_view(MSSQL*, const char*, FILE*);
extern char* get_proc_name(MSSQL*);
extern int dump_proc(MSSQL*, const char*, FILE*);
extern int dump_udts(MSSQL*, FILE*);
extern int dump_relations(MSSQL*, const char*, FILE*);
extern FILE* create_log(const char*);
extern void write_log(FILE*, const char*, ...);
extern void close_log(FILE*);

#endif /* whole file */
