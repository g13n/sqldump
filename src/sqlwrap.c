/*
	Copyright (C) 2001-2002 Gopalarathnam V. <gopal@developercentral.org>.
	
	This file is a part of SqlDump.
	
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with SqlDump; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "sqlwrap.h"
#include "config.h"
#include <stdarg.h>				/* must be after stdio.h */

bool dumpuser = true;			/* dump database users */
bool adddrop = false;			/* add drop table statements */
bool dumpdefn = true;			/* dump table definitions */
bool dumpdata = true;			/* dump table data */
bool dumpprocs = true;			/* dump stored procedures */
bool dumpviews = true;			/* dump views */
bool dumpudts = true;			/* dump user-defined types */
bool listserv = false;			/* list registered sql servers */
char* where = 0;				/* query condition to be matched */

bool allok;						/* 1 if everything was successful! */
FILE* lf;						/* log file pointer */
extern char log_file[MAX_PATH];	/* log file name */

bool identity = false;			/* 1 if current table has an identity column */
char fldbuf[SQLROWSIZ];			/* field names separated by comma */

void write_header(const char* name, MSSQL* mssql, FILE* sql_file)
/* Output startup banner. */
{
	fprintf(sql_file, "-- %s Ver %s (libmssql version %s)\n--\n",
			name, DUMP_VERSION, LIB_VERSION);
	fprintf(sql_file, "-- Host: %s	  Database: %s\n",
			mssql_host(mssql), mssql_database(mssql));
	fputs("-- ---------------------------------------------------------\n",
			sql_file);
	fprintf(sql_file, "-- Server version\t%s\n", mssql_get_server_info(mssql));
	fprintf(sql_file, "\n");
	fflush(sql_file);
}

MSSQL* serverconnect(char* host, char* user, char* passwd, char* db)
/* Attempt connection with Microsoft SQL Server. */
{
	MSSQL* local_mssql;
	
	if ((local_mssql = mssql_connect(host, user, passwd)) == 0) return 0;
	if (db && mssql_select_db(local_mssql, db) < 0) return 0;
	return local_mssql;
}

void serverdisconnect(MSSQL* mssql)	/* shouldn't this be a macro? */
/* Disconnect from Microsoft SQL Server. */
{
	mssql_close(mssql);
}

ulong space_used(MSSQL* mssql, const char* table)
/*
	Determine the table size in Kilobytes if table is not NULL. If table is
	NULL, the total size of the database (in Kilobytes) is returned.
	The table parameter is unused now.
*/
{
	MSSQL_RES* res;
	MSSQL_COL p;
	ulong dbsz;

	dbsz = 0L;					/* return value incase of an error */
	if (mssql_query(mssql, "EXECUTE sp_spaceused") < 0) return dbsz;
	if ((res = mssql_use_result(mssql)) == 0) return dbsz;
	if (mssql_fetch_row(res)) {
		p = mssql_fetch_col(res, 2); /* fetch the "size" column */
		dbsz = strtod(p, 0) * 1024;
	}
	mssql_flush(res->handle);	/* throw away the result, we're done */
	UNREFERENCED_PARAMETER(table);
	return dbsz;
}

char* get_table_name(MSSQL* mssql)
/*
	Get the list of tables in the current database. Return table name or NULL
	if there aren't any more tables.
*/
{
	MSSQL_COL p;
	static MSSQL_RES* res = 0;

	if (!res && (res = mssql_list_tables(mssql, 0)) == 0) return 0;
next:
	if (mssql_fetch_row(res)) {
		p = mssql_fetch_col(res, SHOW_TABLENAME);
		if (strcmp(p, "dtproperties") == 0)
			goto next;
		return p;				/* return table name */
	}
	return 0;
}

int get_table_structure(MSSQL* mssql, const char* table, FILE* sql_file)
/*
	Output the table structure, along with `drop' statements and indexes. The
	total number of columns in the table is returned. A value of -1 is returned
	to indicate a failure.
*/
{
	MSSQL* local_mssql;
	MSSQL_RES* res;
	uint i, nr_fields;
	char buff[BUFSIZ];
	char tname[BUFSIZ];
	char tblbuf[32*BUFSIZ];		/* possible candidate for buffer overflow! */

	tblbuf[0] = 0;
	escape(tname, table);
	if ((local_mssql = serverconnect(mssql_host(mssql), mssql_user(mssql),
				  mssql_passwd(mssql), mssql_database(mssql))) == 0) return -1;
	write_log(lf, ". exporting table %31s", tname);
	/*
		I couldn't think of any other way to extract the number of fields in
		a table. I could have used mssql_list_fields() and retrieved the number
		of rows, but it _doesn't_ work. so until I fix that, this piece of code
		will do the magic.
	*/
	sprintf(buff, "select * from %s", tname);
	if (mssql_query(local_mssql, buff) < 0) return -1;
	if ((res = mssql_use_result(local_mssql)) == 0) return -1;
	nr_fields = mssql_num_fields(res);
	mssql_flush(res->handle);	/* discard the result */
	sprintf(buff, "\n-- Table structure for table '%s'\n", tname);
	strcat(tblbuf, buff);
	if (adddrop) {
		sprintf(buff, "IF EXISTS (SELECT * FROM sysobjects "
				"WHERE (name = '%s')) DROP TABLE %s\nGO\n", table, tname);
		strcat(tblbuf, buff);
	}
	if ((res = mssql_list_fields(local_mssql, table, 0)) == 0) return -1;
	identity = 0;				/* off by default */
	memset(fldbuf, 0, sizeof(fldbuf));
	sprintf(buff, "CREATE TABLE %s (\n", tname);
	strcat(tblbuf, buff);
	for (i = 1; mssql_fetch_row(res); ++i) {
		MSSQL_COL p, name;
		struct sqltypes* k;
		char tmpbuf[BUFSIZ];

		escape(tmpbuf, mssql_fetch_col(res, SHOW_FIELDNAME));
		strcpy(buff, tmpbuf);
		strcat(tblbuf, buff);
		strcat(fldbuf, buff);
		if (i < nr_fields) strcat(fldbuf, ", ");
		name = mssql_fetch_col(res, SHOW_TYPE);
		sprintf(buff, " %s", firstword(name));
		strcat(tblbuf, buff);
		buff[0] = 0;
		if ((k = type_lookup(name)) != 0) {
			if (k->prec) {
				if (k->scale) {
					sprintf(buff, "(%s,%s)",
							mssql_fetch_col(res, SHOW_PRECISION),
							mssql_fetch_col(res, SHOW_SCALE));
				}
				else {
					sprintf(buff, "(%s)",
							mssql_fetch_col(res, SHOW_PRECISION));
				}
			}
		}
		if (strstr(name, "identity") != 0) {
			identity = 1;
			strcat(buff, " IDENTITY");
		}
		if (buff[0]) strcat(tblbuf, buff);
		/*
			Uncomment the following lines for SQL7.0 style constraints.
			
		p = mssql_fetch_col(res, SHOW_DEFAULT);
		if (p[0]) {
			sprintf(buff, " DEFAULT %s", p);
			strcat(tblbuf, buff);
		}
		*/
		if (!atoi(mssql_fetch_col(res, SHOW_NULL)))
			strcat(tblbuf, " NOT NULL");
		sprintf(buff, (i < nr_fields) ? ",\n" : ")\n");
		strcat(tblbuf, buff);
	}
	strcat(tblbuf, "GO\n\n");
	mssql_flush(res->handle);	/* cancel pending results; we're done */
	if (!dumpdefn) goto done;	/* CREATE TABLE statements not necessary! */
	fputs(tblbuf, sql_file);
	if ((res = mssql_list_indexes(local_mssql, table)) == 0) return nr_fields;
	while (mssql_fetch_row(res)) {
		MSSQL_COL p;
		
		p = mssql_fetch_col(res, SHOW_INDEXDESC);
		/*
			Don't script indexes automatically created by Microsoft SQL Server.
		*/
		if (strstr(p, "auto create") == 0) {
			sprintf(buff, "CREATE %s %s INDEX %s ON %s (%s)\nGO\n\n",
					(strstr(p, "unique") != 0) ? "UNIQUE" : "",
					(strstr(p, "nonclustered") != 0)
					? "NONCLUSTERED" : "CLUSTERED",
					mssql_fetch_col(res, SHOW_INDEXNAME), tname,
					mssql_fetch_col(res, SHOW_INDEXKEYS));
			fputs(buff, sql_file);
		}
	}
	mssql_flush(res->handle);
done:
	serverdisconnect(local_mssql);
	fflush(sql_file);
	return nr_fields;
}

unsigned long dump_table(MSSQL* mssql, const char* table, uint fields,
	FILE* sql_file)
/* Dump table data. Return number of rows dumped. */
{
	MSSQL* local_mssql;
	char sqlbuf[BUFSIZ];
	MSSQL_RES* res;
	char tname[BUFSIZ];
    unsigned long n = 0;		/* # rows exported */

	escape(tname, table);
	if ((local_mssql = serverconnect(mssql_host(mssql), mssql_user(mssql),
				  mssql_passwd(mssql), mssql_database(mssql))) == 0) goto done;
	fprintf(sql_file, "\n-- Dumping data for table '%s'\n", tname);
	sprintf(sqlbuf, "SELECT * FROM %s", tname);
	if (where) {
		fprintf(sql_file, "-- WHERE: %s\n", where);
		strcat(sqlbuf, " WHERE ");
		strcat(sqlbuf, where);
	}
	fputs("--\n", sql_file);
	if (identity) {
		fputs("\n-- Enable identity insert\n", sql_file);
		fprintf(sql_file, "SET IDENTITY_INSERT %s ON\nGO\n\n", tname);
	}
	if (mssql_query(local_mssql, sqlbuf) < 0) goto done;
	if ((res = mssql_use_result(local_mssql)) == 0) goto done;
	while (mssql_fetch_row(res)) {
		uint fld;
		MSSQL_FIELD* p;

		fprintf(sql_file, "INSERT INTO %s (%s)\nVALUES(", tname, fldbuf);
		for (fld=0, p=res->fields; fld < fields; ++fld, ++p) {
			MSSQL_COL q;

			q = mssql_fetch_col(res, (uint) fld);
			if (q[0]) {
				if (strstr(p->stype, "char") != 0
				 || strstr(p->stype, "date") != 0
				 || strstr(p->stype, "text") != 0) {
					char* es = (char*) malloc(strlen(q) * 2 + 1);
					quote(es, q);
					fprintf(sql_file, "'%s'", es);
					free(es);
				}
				else if (strstr(p->stype, "binary") != 0) {
					char* es = (char*) malloc(strlen(q) * 2 + 1);
					quote(es, q);
					fprintf(sql_file, "0x%s", es);
					free(es);
				}
				else if (strstr(p->stype, "money") != 0)
					fprintf(sql_file, "%g", strtod(q, 0));
				else
					fputs(q, sql_file);
			}
			else				/* column has null value */
				fputs("NULL", sql_file);
			if (fld < fields - 1) fputs(", ", sql_file);
		}
		fputs(")\nGO\n", sql_file);
		n++;
	}
	if (identity) {
		fputs("\n-- Disable identity insert\n", sql_file);
		fprintf(sql_file, "SET IDENTITY_INSERT %s OFF\nGO\n\n", tname);
	}
	fputs("\n", sql_file);
	fflush(sql_file);
 done:
	mssql_flush(res->handle);
	serverdisconnect(local_mssql);
	write_log(lf, "%8ld   rows exported\n", n);
	return n;
}

int dump_users(MSSQL* mssql, FILE* sql_file)
/* Dump the list of logins in the current database. */
{
	MSSQL_RES* res;
	int n;

	n = 0;						/* # users added */
	if ((res = mssql_list_users(mssql)) == 0) return -1;
	fprintf(sql_file, "\n-- Dumping users in '%s'\n", mssql_database(mssql));
	write_log(lf, ". exporting database logins");
	while (mssql_fetch_row(res)) {
		MSSQL_COL p;
		char temp[BUFSIZ];
		
		p = mssql_fetch_col(res, SHOW_LOGIN);
		if (p && p[0]
		 && strcmp(p, "sa")!=0 && strcmp(p, "guest")!=0) {
			sprintf(temp, "IF NOT EXISTS (SELECT * FROM master.dbo.syslogins "
					"WHERE (name = '%s')) EXECUTE sp_addlogin '%s'\nGO\n",
					p, p);
			fputs(temp, sql_file);
			sprintf(temp, "IF NOT EXISTS (SELECT * FROM sysusers "
					"WHERE (name = '%s')) "
					"EXECUTE sp_adduser '%s', '%s', '%s'\nGO\n",
					p, p, mssql_fetch_col(res, SHOW_USERNAME),
					mssql_fetch_col(res, SHOW_GROUP));
			fputs(temp, sql_file);
			sprintf(temp, "EXECUTE sp_defaultdb '%s', '%s'\nGO\n\n", p,
					mssql_fetch_col(res, SHOW_DATABASE));
			fputs(temp, sql_file);
			++n;
		}
	}
	mssql_flush(res->handle);
	write_log(lf, "%30d logins exported\n", n);
	return n;
}

char* get_view_name(MSSQL* mssql)
/*
	Get the list of views in the current database. Return view name or NULL if
	there aren't any more views.
*/
{
	static MSSQL_RES* res = 0;

	if (!res && (res = mssql_list_views(mssql, 0)) == 0) return 0;
	if (mssql_fetch_row(res))
		return mssql_fetch_col(res, SHOW_VIEWNAME);
	return 0;
}

int dump_view(MSSQL* mssql, const char* view, FILE* sql_file)
/* Dump the definition of the view `view'. */
{
	MSSQL* local_mssql;
	MSSQL_RES* res;
	char temp[BUFSIZ];
	char vname[BUFSIZ];

	escape(vname, view);
	if ((local_mssql = serverconnect(mssql_host(mssql), mssql_user(mssql),
				  mssql_passwd(mssql), mssql_database(mssql))) == 0) return -1;
	sprintf(temp, "EXECUTE sp_helptext %s", vname);
	if (mssql_query(local_mssql, temp) < 0) return -1;
	if ((res = mssql_use_result(local_mssql)) == 0) return -1;
	fprintf(sql_file, "IF EXISTS (SELECT * FROM sysobjects "
			"WHERE (name = '%s') AND (xtype = 'V')) DROP VIEW %s\nGO\n",
			view, vname);
	while (mssql_fetch_row(res))
		fprintf(sql_file, "%s", mssql_fetch_col(res, 0));
	fprintf(sql_file, "\nGO\n\n");
	mssql_flush(res->handle);
	serverdisconnect(local_mssql);
	return 0;
}

char* get_proc_name(MSSQL* mssql)
/*
	Get the list of stored procedures in the current database. Return procedure
	name or NULL if there aren't any more procedures.
*/
{
	static MSSQL_RES* res = 0;

	if (!res && (res = mssql_list_procs(mssql, 0)) == 0) return 0;
	if (mssql_fetch_row(res)) {
		MSSQL_COL p;
		
		p = mssql_fetch_col(res, SHOW_PROCNAME);
		*(strchr(p, ';')) = 0;
		return p;
	}
	return 0;
}

int dump_proc(MSSQL* mssql, const char* proc, FILE* sql_file)
/* Dump the definition of the stored procedure `proc'. */
{
	MSSQL* local_mssql;
	MSSQL_RES* res;
	char temp[BUFSIZ];
	char pname[BUFSIZ];

	escape(pname, proc);
	if ((local_mssql = serverconnect(mssql_host(mssql), mssql_user(mssql),
				  mssql_passwd(mssql), mssql_database(mssql))) == 0) return -1;
	sprintf(temp, "EXECUTE sp_helptext %s", pname);
	if (mssql_query(local_mssql, temp) < 0) return -1;
	if ((res = mssql_use_result(local_mssql)) == 0) return -1;
	fprintf(sql_file, "IF EXISTS (SELECT * FROM sysobjects "
			"WHERE (name = '%s') AND (xtype = 'P')) DROP PROCEDURE %s\nGO\n",
			proc, pname);
	while (mssql_fetch_row(res))
		fprintf(sql_file, "%s", mssql_fetch_col(res, 0));
	fprintf(sql_file, "\nGO\n\n");
	mssql_flush(res->handle);
	serverdisconnect(local_mssql);
	return 0;
}

int dump_udts(MSSQL* mssql, FILE* sql_file)
/* Dump the list of user-defined data types for the current database. */
{
	MSSQL_RES* res;
	int n;

	n = 0;						/* # udts dumped */
	if ((res = mssql_list_udts(mssql)) == 0) return -1;
	fprintf(sql_file, "\n-- Dumping user-defined datatypes in '%s'\n",
			mssql_database(mssql));
	write_log(lf, ". exporting user-defined types");
	while (mssql_fetch_row(res)) {
		MSSQL_COL name, type;
		int nullval;
		struct sqltypes* k;
		char buf[BUFSIZ], temp[16];
		
		name = mssql_fetch_col(res, SHOW_UDTNAME);
		type = mssql_fetch_col(res, SHOW_UDTTYPE);
		nullval = atoi(mssql_fetch_col(res, SHOW_UDTNULL));
		if (strcmp(name, "sysname") != 0) { /* sysname is built-in datatype */
			char ename[BUFSIZ];	/* [escaped] type name */

			escape(ename, name);
			fprintf(sql_file, "IF EXISTS (SELECT * FROM systypes "
					"WHERE (name = '%s')) EXECUTE sp_droptype '%s'\nGO\n",
					name, name);
			sprintf(buf, "EXECUTE sp_addtype %s, '%s", ename, type);
			if ((k = type_lookup(type)) >= 0) {
				if (k->prec) {
					if (k->scale) {
						sprintf(temp, "(%s,%s)'",
								mssql_fetch_col(res, SHOW_UDTPREC),
								mssql_fetch_col(res, SHOW_UDTSCALE));
					}
					else {
						sprintf(temp, "(%s)'",
								mssql_fetch_col(res, SHOW_UDTPREC));
					}
				}
				else
					sprintf(temp, "'");
			}
			strcat(buf, temp);
			if (nullval)
				strcat(buf, ", 'NULL'");
			else
				strcat(buf, ", 'NOT NULL'");
			strcat(buf, "\nGO\n");
			fputs(buf, sql_file);
			++n;
		}
	}
	mssql_flush(res->handle);
	fputs("\n", sql_file);
	write_log(lf, "%27d  types exported\n", n);
	return n;
}

int dump_relations(MSSQL* mssql, const char* table, FILE* sql_file)
/* Dump foreign keys, relationships for the table `table'. */
{
	MSSQL* local_mssql;
	MSSQL_RES* res;
	char tname[BUFSIZ];
	int n;

	n = 0;
	escape(tname, table);
	if ((local_mssql = serverconnect(mssql_host(mssql), mssql_user(mssql),
				  mssql_passwd(mssql), mssql_database(mssql))) == 0) return -1;
	fprintf(sql_file, "\n-- Dumping foreign keys on table '%s'\n", tname);
	if ((res = mssql_list_fkeys(local_mssql, tname)) == 0) return -1;
	while (mssql_fetch_row(res)) {
		char temp[BUFSIZ];
		
		sprintf(temp, "ALTER TABLE %s ADD CONSTRAINT %s"
				" FOREIGN KEY (%s) REFERENCES %s(%s)\nGO\n\n",
				mssql_fetch_col(res, SHOW_FKTABLE),
				mssql_fetch_col(res, SHOW_FKNAME),
				mssql_fetch_col(res, SHOW_FKCOLNAME),
				mssql_fetch_col(res, SHOW_PKTABLE),
				mssql_fetch_col(res, SHOW_PKCOLNAME));
		fputs(temp, sql_file);
		++n;
	}
	mssql_flush(res->handle);
	serverdisconnect(local_mssql);
	return n;
}

FILE* create_log(const char* dbname)
/*
	Create the log file named dbname.log in the temporary directory (usually
	C:\TEMP or C:\WINDOWS\TEMP or as defined in the configuration). If the
	creation was successful, a file pointer is returned, otherwise NULL is
	returned.
*/
{
	FILE* lf;

	if (!GetTempPath(MAX_PATH, log_file)) /* failure getting temp path */
		strcpy(log_file, "C:\\"); /* assume root drive has enough space */
	strcat(log_file, dbname);
	strcat(log_file, ".log");
	if ((lf = fopen(log_file, "w")) == 0) return 0;
	return lf;					/* return pointer to file struct */
}

void write_log(FILE* lf, const char* fmt, ...)
/*
	Write the formatted message to the log file that was previously
	created/opened using create_log().
	This function is analagous to the standard library function fprintf(3).
*/
{
	if (lf) {
		va_list ap;
		
		va_start(ap, fmt);
		vfprintf(lf, fmt, ap);
		va_end(ap);
	}
}

void close_log(FILE* lf)
/* Flush and close the log file created/opened by create_log(). */
{
	if (lf) {
		fflush(lf);				/* actually not needed though! */
		fclose(lf);
	}
}
