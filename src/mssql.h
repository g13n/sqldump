/*
	mssql.h - definitions for the libmssql library
	
	Copyright (C) 2001-2002 Gopalarathnam V. <gopal@developercentral.org>.
	
	This library is free software; you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public
	License as published by the Free Software Foundation; either
	version 2.1 of the License, or (at your option) any later version.
	
	This library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	Lesser General Public License for more details.
	
	You should have received a copy of the GNU Lesser General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>				/* include the "standard" headers */
#include <string.h>
#include <stdlib.h>

#include <windows.h>

#define DBNTWIN32				/* must identify operating system */

#include "sqlfront.h"
#include "sqldb.h"

#ifndef _MSSQL_H
#define _MSSQL_H

#define LIB_VERSION "2.3"		/* library version */

#ifndef BUFSIZ
#define BUFSIZ      1024
#endif

#define SQLROWSIZ   8060		/* max size of one row */

/* typedef char bool;				/\* a boolean type *\/ */
typedef unsigned int uint;
typedef unsigned long ulong;

/* enum { false, true };			/\* boolean flags *\/ */

extern char last_error[];

typedef struct mssql_field {	/* database column: */
	char* name;					/* name of column */
	char* table;				/* table of column if column was a field */
	char* def;					/* default value (set by mssql_list_fields) */
	uint type;					/* type of field */
	char* stype;				/* type of field as readable string */
	uint maxlength;				/* maximum length of the field */
	int precision;				/* precision if type is decimal */
	int scale;					/* scale if type is decimal */
	int null;					/* 1 if nullable, 0 otherwise */
	int identity;				/* 1 if identity column, 0 otherwise */
} MSSQL_FIELD;

typedef char* MSSQL_ROW;		/* return data as string */
typedef char* MSSQL_COL;		/* return data as string */

typedef struct mssql {			/* database connection: */
	char* host;					/* host name of database server */
	char* user;					/* user name to connect as */
	char* passwd;				/* password */
	char* db;					/* last database opened */
	char* server_version;		/* version of database server */
	uint field_count;			/* number of fields fetched by last query */
	ulong affected_rows;		/* number of rows affected by last query */
	MSSQL_FIELD* fields;		/* list of fields */
	DBPROCESS* dbproc;			/* dbprocess handle */
} MSSQL;

typedef struct mssql_res {		/* query result: */
	ulong row_count;			/* number of rows affected */
	uint field_count;			/* number of columns fetched */
	MSSQL_FIELD* fields;		/* list of columns */
	char row[SQLROWSIZ];		/* the entire row */
	uint rowlen;				/* length of this row */
	ulong* lengths;				/* column lengths of current row */
	MSSQL* handle;				/* connection identifier */
	int eof;					/* used by mssql_fetch_row */
} MSSQL_RES;

/* Some useful function macros */

#define mssql_get_server_info(mssql)           (mssql)->server_version
#define get_last_error()                       last_error

#define mssql_num_rows(res)                    (res)->row_count
#define mssql_num_fields(res)                  (res)->field_count
#define mssql_eof(res)                         (res)->eof
#define mssql_fetch_field_direct(res, fieldnr) (&(res)->fields[fieldnr])
#define mssql_fetch_fields(res)                (res)->fields

#define mssql_field_count(mssql)               (mssql)->field_count
#define mssql_affected_rows(mssql)             (mssql)->affected_rows
#define mssql_host(mssql)                      (mssql)->host
#define mssql_database(mssql)                  (mssql)->db
#define mssql_user(mssql)                      (mssql)->user
#define mssql_passwd(mssql)                    (mssql)->passwd

int mssql_msg_handler(DBPROCESS* dbproc, DBINT msgno, int msgstate,
		int severity, const char* msgtext, const char* srvname,
		const char* procname, DBUSMALLINT line);
MSSQL* mssql_connect(char* host, char* user, char* passwd);
void mssql_close(MSSQL* mssql);
int mssql_select_db(MSSQL* mssql, const char* db);
int mssql_query(MSSQL *mssql, const char* q);
MSSQL_RES* mssql_list_dbs(MSSQL* mssql);
MSSQL_RES* mssql_list_tables(MSSQL* mssql, const char* wild);
MSSQL_RES* mssql_list_fields(MSSQL* mssql, const char* table,
		const char* wild);
MSSQL_RES* mssql_list_indexes(MSSQL* mssql, const char* table);
MSSQL_RES* mssql_list_fkeys(MSSQL* mssql, const char* table);
MSSQL_RES* mssql_list_users(MSSQL* mssql);
MSSQL_RES* mssql_list_views(MSSQL* mssql, const char* wild);
MSSQL_RES* mssql_list_procs(MSSQL* mssql, const char* wild);
MSSQL_RES* mssql_list_udts(MSSQL* mssql);
short mssql_list_servers(char*** servers);
MSSQL_RES* mssql_use_result(MSSQL* mssql);
int read_one_row(MSSQL_RES* res);
MSSQL_ROW mssql_fetch_row(MSSQL_RES* result);
char* mssql_fetch_col(MSSQL_RES* res, uint col);
void mssql_flush(MSSQL* mssql);

#endif /* whole file */
