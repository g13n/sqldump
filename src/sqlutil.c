/*
	Copyright (C) 2001-2002 Gopalarathnam V. <gopal@developercentral.org>.
	
	This file is a part of SqlDump.
	
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with SqlDump; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "sqlutil.h"

struct sqltypes keytab[] = {	/* list of Microsoft SQL Server data types */
	{ "binary", false, false },
	{ "bit", false, false },
	{ "char", true, false },
	{ "datetime", false, false },
	{ "decimal", true, true },
	{ "float", true, false },
	{ "image", false, false },
	{ "int", false, false },
	{ "money", false, false },
	{ "nchar", true, false },
	{ "ntext", false, false },
	{ "nvarchar", true, false },
	{ "numeric", true, true },
	{ "real", false, false },
	{ "smalldatetime", false, false },
	{ "smallint", false, false },
	{ "smallmoney", true, true },
	{ "sysname", false, false },
	{ "text", false, false },
	{ "timestamp", false, false },
	{ "tinyint", false, false },
	{ "uniqueidentifier", false, false },
	{ "varbinary", true, false },
	{ "varchar", true, false }
};
#define NKEYS (sizeof keytab / sizeof(keytab[0]))

const char* mssql_words[] = {
	"ADD", "ALL", "ALTER", "AND", "ANY", "AS", "ASC", "AUTHORIZATION", "AVG",
	"BACKUP", "BEGIN", "BETWEEN", "BREAK", "BROWSE", "BULK", "BY",
	"CASCADE", "CASE", "CHECK", "CHECKPOINT", "CLOSE", "CLUSTERED", "COALESCE",
	"COLUMN", "COMMIT", "COMMITTED", "COMPUTE", "CONFIRM", "CONSTRAINT",
	"CONTAINS", "CONTAINSTABLE", "CONTINUE", "CONTROLROW", "CONVERT", "COUNT",
	"CREATE", "CROSS", "CURRENT", "CURRENT_DATE", "CURRENT_TIME",
	"CURRENT_TIMESTAMP", "CURRENT_USER", "CURSOR",
	"DATABASE", "DBCC", "DEALLOCATE", "DECLARE", "DEFAULT", "DELETE", "DENY",
	"DESC", "DISK", "DISTINCT", "DISTRIBUTED", "DOUBLE", "DROP", "DUMMY",
	"DUMP",
	"ELSE", "END", "ERRLVL", "ERROREXIT", "ESCAPE", "EXCEPT", "EXEC",
	"EXECUTE", "EXISTS", "EXIT",
	"FETCH", "FILE", "FILLFACTOR", "FLOPPY", "FOR", "FOREIGN", "FREETEXT",
	"FREETEXTTABLE", "FROM", "FULL",
	"GOTO", "GRANT", "GROUP",
	"HAVING", "HOLDLOCK",
	"IDENTITY", "IDENTITY_INSERT", "IDENTITYCOL", "IF", "IN", "INDEX", "INNER",
	"INSERT", "INTERSECT", "INTO", "IS", "ISOLATION",
	"JOIN",
	"KEY", "KILL",
	"LEFT", "LEVEL", "LIKE", "LINENO", "LOAD",
	"MAX", "MIN", "MIRROREXIT",
	"NATIONAL", "NOCHECK", "NONCLUSTERED", "NOT", "NULL", "NULLIF",
	"OF", "OFF", "OFFSETS", "ON", "ONCE", "ONLY", "OPEN", "OPENDATASOURCE",
	"OPENQUERY", "OPENROWSET", "OPTION", "OR", "ORDER", "OUTER", "OVER",
	"PERCENT", "PERM", "PERMANENT", "PIPE", "PLAN", "PRECISION", "PREPARE",
	"PRIMARY", "PRINT", "PRIVILEGES", "PROC", "PROCEDURE", "PROCESSEXIT",
	"PUBLIC",
	"RAISERROR", "READ", "READTEXT", "RECONFIGURE", "REFERENCES", "REPEATABLE",
	"REPLICATION", "RESTORE", "RESTRICT", "RETURN", "REVOKE", "RIGHT",
	"ROLLBACK", "ROWCOUNT", "ROWGUIDCOL", "RULE",
	"SAVE", "SCHEMA", "SELECT", "SERIALIZABLE", "SESSION_USER", "SET",
	"SETUSER", "SHUTDOWN", "SOME", "STATISTICS", "SUM", "SYSTEM_USER",
	"TABLE", "TAPE", "TEMP", "TEMPORARY", "TEXTSIZE", "THEN", "TO", "TOP",
	"TRAN", "TRANSACTION", "TRIGGER", "TRUNCATE", "TSEQUAL",
	"UNCOMMITTED", "UNION", "UNIQUE", "UPDATE", "UPDATETEXT", "USE", "USER",
	"VALUES", "VARYING", "VIEW",
	"WAITFOR", "WHEN", "WHERE", "WHILE", "WITH", "WORK", "WRITETEXT"
};
#define NWORDS (sizeof mssql_words / sizeof(mssql_words[0]))

struct tab* tablist = 0;		/* list of tables in current database */

char* quote(char* dst, const char* src)
/* Return a quoted string. */
{
	char* p;

	for (p = dst; *src; ++p, ++src) {
		if (*src == '\'') *p++ = *src;
		*p = *src;
	}
	*p = 0;
	return dst;
}

char* firstword(const char* s)
/* Return the first word from s. */
{
	char w[BUFSIZ];
	char* p = w;

	while (*s) {
		if (!isalnum(*s)) break;
		*p++ = *s++;
	}
	*p = 0;
	return strdup(w);
}

int strlcmp(const char* s, const char* t)
/*
	Perform lower case comparison of strings s and t. Return >0,0,<0 if s>t,
	s==t, s<t respectively.
*/
{
	for ( ; tolower(*s) == tolower(*t); s++, t++)
		if (*s == 0) break;
	return tolower(*s) - tolower(*t);
}

struct sqltypes* type_lookup(const char* name)
/* Find name in sqltypes[0]...sqltypes[n-1]. */
{
	int cond;
	int low, high, mid;

	low = 0;
	high = NKEYS - 1;
	while (low <= high) {
		mid = (low+high) / 2;
		if ((cond = strlcmp(name, keytab[mid].name)) < 0)
			high = mid - 1;
		else if (cond > 0)
			low = mid + 1;
		else
			return &keytab[mid];
	}
	return 0;
}

bool is_keyword(const char* w)
/*
	Return true if w is a Microsoft SQL Server keyword, otherwise return false.
*/
{
	int cond;
	int low, high, mid;

	low = 0;
	high = NWORDS - 1;
	while (low <= high) {
		mid = (low+high) / 2;
		if ((cond = strlcmp(w, mssql_words[mid])) < 0)
			high = mid - 1;
		else if (cond > 0)
			low = mid + 1;
		else
			return true;
	}
	return false;
}

char* escape(char* dst, const char* src)
/* Escape object name using T-SQL bracket ([name]) syntax if necessary. */
{
	if (strchr(src, ' ') != 0 || is_keyword(src))
		sprintf(dst, "[%s]", src);
	else
		strcpy(dst, src);
	return dst;
}

bool exists(const char* file)
/* Return true if `file' exists, otherwise return false. */
{
	FILE* fp;
	bool rc;

	rc = (fp = fopen(file, "r")) != 0;
	if (fp) fclose(fp);
	return rc;
}

bool install(const char* name, uint n)
/*
	Install table entry (name, n) in the list of tables. Return true on
	success, false otherwise. The new node is always appended at the end of
	list.
*/
{
	struct tab* p = (struct tab*) malloc(sizeof(struct tab));
	if (!p) return false;

	p->name = strdup(name);		/* build a new node */
	p->cols = n;
	p->link = 0;
	
	if (!tablist) {				/* the list is empty, create first entry */
		tablist = p;
		return true;
	}
	else {
		struct tab* q;
		struct tab* r;
		
		for (q = tablist; q; r = q, q = q->link) /* go to the end of list */
			;
		r->link = p;			/* append node */
	}
	return true;
}

void freetablist()
/* Frees the tab list that has been built. */
{
	struct tab* p;
	struct tab* q;

	for (q=tablist, p=q->link; p; q=p, p=p->link)
		free(q);
}
