/* 
 * Copyright (c) 2001-2002
 * Gopalarathnam <gopal@developercentral.org>. All rights reserved.
 * 
 * This file is a part of SqlDump.
 * 
 * SqlDump is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * SqlDump is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SqlDump; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <conio.h>
#include <string.h>
#include "sqldump.h"

/* getpass:  read password from terminal and return it */
char *getpass(char *prompt)
{
	int c;
	char buf[64];		/* possibility of a buffer overflow here! */
	char *bufp = buf;

	if (prompt)
		cputs(prompt);	/* output prompt */
	else
		cputs("Password:");
	for (;;) {			/* loop forever */
		c = _getch();	/* read a character from terminal */
		/* If this is a backspace character, back off and erase the previous
			character. Watchout for the beginning of the buffer. */
		if (c == '\b') {
			if (bufp != buf)
				--bufp;
			continue;
		}
		/* If this is a newline or a Ctrl+C, break out of the loop. */
		if (c == '\n' || c == '\r' || c == '\x3')
			break;
		/* ...or copy the character to the buffer. */
		*bufp++ = (char) c;
	}
	/* Terminate buffer and output a newline assuming the user would have
		hit an ENTER. */
	*bufp = '\0';
	if (c == '\n' || c == '\r')
		putchar('\n');
	return strdup(buf);
}
