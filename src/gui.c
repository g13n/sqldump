/*
	Copyright (C) 2001-2002 Gopalarathnam V. <gopal@developercentral.org>.
	
	This file is a part of SqlDump.
	
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with SqlDump; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <windows.h>
#include <prsht.h>				/* requires declarations from windows.h */
#include <commdlg.h>
#include "sqldumpw.h"
#include "sqlwrap.h"
#include "res.h"

#define LASTPAGE 5				/* number of wizard pages */
#define IsButtonState(w, b, v) IsDlgButtonChecked((w), (b)) == (v)

typedef struct _SHAREDWIZDATA {
	HFONT title_font;
	HFONT heading_font;
} SHAREDWIZDATA;
typedef SHAREDWIZDATA* LPSHAREDWIZDATA;

char cancelled = 0;				/* 1 if the wizard has been cancelled */
char loaded = 0;				/* 1 if the of SQL servers have been loaded */
char about_btntext[BUFSIZ];		/* button text for About SqlDump button */
extern char output_file[MAX_PATH]; /* output file name */

void center_window(HWND win)
/* Align window position to almost center of the screen. */
{
	RECT rc;
	
	GetWindowRect(win, &rc);
	SetWindowPos(win, 0,
			(GetSystemMetrics(SM_CXSCREEN)-(rc.right-rc.left)) / 2,
			(GetSystemMetrics(SM_CYSCREEN)-(rc.bottom-rc.top)) / 3,
			0, 0, SWP_NOSIZE | SWP_NOZORDER);
}

bool load_dblist(char* host, char* user, char* passwd, HWND cwin)
/*
	Load the list of databases in Microsoft SQL Server running at host `host'
	in to the list (combo) box `cwin'.
*/
{
	MSSQL* mssql;
	MSSQL_RES* res;

	if ((mssql = serverconnect(host, user, passwd, 0)) == 0) return false;
	if ((res = mssql_list_dbs(mssql)) == 0) return false;
	while (mssql_fetch_row(res)) /* add the database to list */
		ComboBox_AddString(cwin, mssql_fetch_col(res, 0));
	ComboBox_SetCurSel(cwin, 0);
	mssql_flush(res->handle);
	serverdisconnect(mssql);
	return true;
}

LRESULT CALLBACK about_box(HWND win, UINT msg, WPARAM wparam, LPARAM lparam)
/* Display the version information for SqlDump. */
{
	LRESULT retcode = 0;

	switch (msg) {
	case WM_INITDIALOG:
		{
			LOGFONT log_font;
			HDC dc;
			HFONT hf;
			NONCLIENTMETRICS ncm = {0};
			char tmp[BUFSIZ];

			sprintf(tmp, "%s Ver %s (libmssql version %s)",
					progname, DUMP_VERSION, LIB_VERSION);
			SetWindowText(GetDlgItem(win, LBL_PROGNAME), tmp);
			sprintf(tmp, "Build options: %s", CONFIG_COMMAND);
			SetWindowText(GetDlgItem(win, LBL_BUILDOPT), tmp);
			
			ncm.cbSize = sizeof(ncm);
			SystemParametersInfo(SPI_GETNONCLIENTMETRICS, 0, &ncm, 0);
			log_font = ncm.lfMessageFont;
			log_font.lfWeight = FW_BOLD;
			dc = GetDC(0);		/* get screen DC */
			hf = CreateFontIndirect(&log_font);
			ReleaseDC(0, dc);
			SetWindowFont(GetDlgItem(win, LBL_PROGNAME), hf, TRUE);
			retcode = 1;
		}
		break;
	case WM_COMMAND:
		EndDialog(win, 0);
		break;
	}
	UNREFERENCED_PARAMETER(wparam);
	return retcode;
}

LRESULT CALLBACK show_intro(HWND win, UINT msg, WPARAM wparam, LPARAM lparam)
/* Display the introduction screen of the wizard to the user. */
{
	LRESULT retcode = 0;
	
	switch (msg) {
	case WM_INITDIALOG:
		{
			LPSHAREDWIZDATA pdata;
			
			/*
				Get the shared data from PROPSHEETPAGE lparam value and load it
				into GWL_USERDATA.
			*/
			pdata = (LPSHAREDWIZDATA) ((LPPROPSHEETPAGE) lparam)->lParam;
			SetWindowFont(GetDlgItem(win, LBL_CAPTION),
					pdata->title_font, TRUE);
			retcode = 1;
		}
		break;
	case WM_NOTIFY:
		{
			LPNMHDR lpnm = (LPNMHDR) lparam;
			switch (lpnm->code) {
			case PSN_SETACTIVE:
				{
					HICON icon;
					
					PropSheet_SetWizButtons(GetParent(win), PSWIZB_NEXT);
					icon = LoadIcon(myapp, MAKEINTRESOURCE(ICO_LOGO));
					SendMessage(lpnm->hwndFrom,
							WM_SETICON, (WPARAM) ICON_BIG, (LPARAM) icon);
					SendMessage(lpnm->hwndFrom,
							WM_SETICON, (WPARAM) ICON_SMALL, (LPARAM) icon);
					SetWindowText(GetDlgItem(GetParent(win), IDHELP),
							about_btntext);
					center_window(lpnm->hwndFrom);
				}
				break;
			case PSN_HELP:		/* show About box */
				DialogBox(myapp, MAKEINTRESOURCE(DLG_ABOUT), lpnm->hwndFrom,
						(DLGPROC) about_box);
				break;
			case PSN_WIZNEXT:	/* handle a next button click here */
				break;
			case PSN_RESET:		/* handle a cancel button click here */
				cancelled = 1;
				break;
			case PSN_QUERYCANCEL: /* confirm quit */
				{
					int rc = MessageBox(win,
							"You have cancelled the operation.\n"
							"Are you sure you wish to quit?", progname,
							MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON2);
					if (rc == IDNO)
						SetWindowLong(win, DWL_MSGRESULT, (long) TRUE);
					else
						SetWindowLong(win, DWL_MSGRESULT, (long) FALSE);
					retcode = 1;
				}
				break;
			default:
				break;
			}
		}
		break;
	default:
		break;
	}
	UNREFERENCED_PARAMETER(wparam);
	return retcode;
}

LRESULT CALLBACK get_connection_settings(HWND win, UINT msg,
		WPARAM wparam, LPARAM lparam)
/*
	Get the connection settings to connect to Microsoft SQL Server from the
	user.
*/
{
	HWND whost, wuser, wdb;
	LONG retcode = 0;

	whost = GetDlgItem(win, CBO_HOST);
	wuser = GetDlgItem(win, TXT_USER);
	wdb = GetDlgItem(win, CBO_DB);
	
	switch (msg) {
	case WM_INITDIALOG:
		{
			LPSHAREDWIZDATA pdata;
			
			/*
				Get the shared data from PROPSHEETPAGE lparam value and load it
				into GWL_USERDATA.
			*/
			pdata = (LPSHAREDWIZDATA) ((LPPROPSHEETPAGE) lparam)->lParam;
			SetWindowLong(win, GWL_USERDATA, (long) pdata);
			SetWindowFont(GetDlgItem(win, LBL_HEADING),
					pdata->heading_font, TRUE);
			SendMessage(whost, CB_SETEXTENDEDUI, (WPARAM) TRUE, 0);
			SendMessage(wdb, CB_SETEXTENDEDUI, (WPARAM) TRUE, 0);
			retcode = 1;
		}
		break;
	case WM_COMMAND:
		{
			bool enable_next, enable_refresh;
			
			switch (LOWORD(wparam)) {
			case CBO_HOST:		/* server list */
				if (HIWORD(wparam) == CBN_EDITCHANGE) {
					enable_next = ComboBox_GetTextLength(whost) > 0
							&& Edit_GetTextLength(wuser) > 0
							&& ComboBox_GetTextLength(wdb) > 0;
					if (enable_next)
						PropSheet_SetWizButtons(GetParent(win),
								PSWIZB_BACK | PSWIZB_NEXT);
					else
						PropSheet_SetWizButtons(GetParent(win), PSWIZB_BACK);
					enable_refresh = ComboBox_GetTextLength(whost) > 0
							&& Edit_GetTextLength(wuser) > 0;
					EnableWindow(GetDlgItem(win, BTN_REFRESH), enable_refresh);
				}
				break;
			case TXT_USER:		/* user name */
				if (HIWORD(wparam) == EN_CHANGE) {
					enable_next = ComboBox_GetTextLength(whost) > 0
							&& Edit_GetTextLength(wuser) > 0
							&& ComboBox_GetTextLength(wdb) > 0;
					if (enable_next)
						PropSheet_SetWizButtons(GetParent(win),
								PSWIZB_BACK | PSWIZB_NEXT);
					else
						PropSheet_SetWizButtons(GetParent(win), PSWIZB_BACK);
					enable_refresh = ComboBox_GetTextLength(whost) > 0
							&& Edit_GetTextLength(wuser) > 0;
					EnableWindow(GetDlgItem(win, BTN_REFRESH), enable_refresh);
				}
				break;
			case CBO_DB:		/* database list */
				if (HIWORD(wparam) == CBN_EDITCHANGE) {
					enable_next = ComboBox_GetTextLength(whost) > 0
							&& Edit_GetTextLength(wuser) > 0
							&& ComboBox_GetTextLength(wdb) > 0;
					if (enable_next)
						PropSheet_SetWizButtons(GetParent(win),
								PSWIZB_BACK | PSWIZB_NEXT);
					else
						PropSheet_SetWizButtons(GetParent(win), PSWIZB_BACK);
				}
				break;
			case BTN_REFRESH:	/* (re)load database list */
				{
					char host[BUFSIZ]; /* Microsoft SQL Server host name */
					char user[BUFSIZ]; /* user name to connect as */
					char passwd[BUFSIZ]; /* password */
					char stmp[BUFSIZ];

					ComboBox_GetText(whost, host, BUFSIZ);
					Edit_GetText(wuser, user, BUFSIZ);
					Edit_GetText(GetDlgItem(win, TXT_PASSWD), passwd, BUFSIZ);
					SetCursor(LoadCursor(0, IDC_WAIT));
					if (load_dblist(host, user, passwd, wdb) == false) {
						sprintf(stmp,
								"An error occurred while attempting to "
								"connect to Microsoft SQL Server running at "
								"'%s'. %s",
								host, get_last_error());
						MessageBox(0, stmp, progname,
								MB_OK | MB_ICONINFORMATION);
					}
					else {
						if (ComboBox_GetCount(wdb) > 0)	/* atleast one item! */
							PropSheet_SetWizButtons(GetParent(win),
									PSWIZB_BACK | PSWIZB_NEXT);
						else
							PropSheet_SetWizButtons(GetParent(win),
									PSWIZB_BACK);
					}
					SetCursor(LoadCursor(0, IDC_ARROW));
				}
				break;
			}
			break;
		}
	case WM_NOTIFY:
		{
			bool enable_next, enable_refresh;
			HANDLE img;
			LPNMHDR lpnm;
			
			enable_next = ComboBox_GetTextLength(whost) > 0
					&& Edit_GetTextLength(wuser) > 0
					&& ComboBox_GetTextLength(wdb) > 0;
			enable_refresh = ComboBox_GetTextLength(whost) > 0
					&& Edit_GetTextLength(wuser) > 0;
			lpnm = (LPNMHDR) lparam;
			switch (lpnm->code) {
			case PSN_SETACTIVE:
				{
					HICON icon;
					char** servers = 0;	/* list of Microsoft SQL Servers */
					
					if (enable_next)
						PropSheet_SetWizButtons(GetParent(win),
								PSWIZB_BACK | PSWIZB_NEXT);
					else
						PropSheet_SetWizButtons(GetParent(win), PSWIZB_BACK);
					EnableWindow(GetDlgItem(win, BTN_REFRESH), enable_refresh);
					icon = LoadIcon(myapp, MAKEINTRESOURCE(ICO_LOGO));
					SendMessage(lpnm->hwndFrom, WM_SETICON,
							(WPARAM) ICON_BIG, (LPARAM) icon);
					SendMessage(lpnm->hwndFrom, WM_SETICON,
							(WPARAM) ICON_SMALL, (LPARAM) icon);
					img = LoadImage(myapp,
							MAKEINTRESOURCE(ICO_WARNING), IMAGE_ICON, 0, 0, 0);
					if (img)
						SendDlgItemMessage(win, PIC_NOTICE,
								STM_SETIMAGE, IMAGE_ICON, (LPARAM) img);
					SetWindowText(GetDlgItem(GetParent(win), IDHELP),
							about_btntext);
					center_window(lpnm->hwndFrom);
					/*
						Add the list of sql servers on the network to the
						`host' combo box.
					*/
					if (!loaded) {
						int i, n;
						
						n = mssql_list_servers(&servers);
						for (i = 0; i < n; ++i)
							ComboBox_AddString(whost, servers[i]);
						ComboBox_SetCurSel(whost, 0);
						loaded = 1;
					}
					break;
				}
			case PSN_HELP:		/* show About box */
				DialogBox(myapp, MAKEINTRESOURCE(DLG_ABOUT), lpnm->hwndFrom,
						(DLGPROC) about_box);
				break;
			case PSN_WIZNEXT:	/* handle a next button click here */
				{
					char host[BUFSIZ]; /* Microsoft SQL Server host name */
					char user[BUFSIZ]; /* user name to connect as */
					char passwd[BUFSIZ]; /* password */
					char db[BUFSIZ]; /* database name (sql escaped--sql7.0+) */
					char stmp[BUFSIZ];

					ComboBox_GetText(whost, host, BUFSIZ);
					Edit_GetText(wuser, user, BUFSIZ);
					Edit_GetText(GetDlgItem(win, TXT_PASSWD), passwd, BUFSIZ);
					Edit_GetText(wdb, stmp, BUFSIZ);
					escape(db, stmp);
					SetCursor(LoadCursor(0, IDC_WAIT));
					mssql = serverconnect(host, user, passwd, db);
					if (!mssql) {
						sprintf(stmp,
								"An error occurred while attempting to "
								"connect to Microsoft SQL Server running at "
								"'%s'. %s",
								host, get_last_error());
						MessageBox(0, stmp, progname,
								MB_OK | MB_ICONINFORMATION);
						SetWindowLong(win, DWL_MSGRESULT, -1L);
						retcode = 1;
						break;
					}
					else
						dbsize = space_used(mssql, 0); /* db size (approx.) */
					SetCursor(LoadCursor(0, IDC_ARROW));
					if ((lf = create_log(stmp)) == 0) {
						char emsg[BUFSIZ];
						sprintf(emsg,
								"An error occurred while creating log file "
								"'%s'. No log will be generated.", stmp);
						MessageBox(0, emsg, progname,
								MB_OK | MB_ICONINFORMATION);
					}
					if (img)
						DestroyIcon((HICON) img); /* not needed anymore! */
				}
				break;
			case PSN_RESET:	   /* handle a cancel button click here */
				cancelled = 1;
				break;
			case PSN_QUERYCANCEL: /* confirm quit */
				{
					int rc = MessageBox(win,
							"You have cancelled the operation.\n"
							"Are you sure you wish to quit?", progname,
							MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON2);
					if (rc == IDNO)
						SetWindowLong(win, DWL_MSGRESULT, (long) TRUE);
					else
						SetWindowLong(win, DWL_MSGRESULT, (long) FALSE);
					retcode = 1;
				}
				break;
			default:
				break;
			}
			break;
		}
	default:
		break;
	}
	return retcode;
}

LRESULT CALLBACK show_options(HWND win, UINT msg, WPARAM wparam, LPARAM lparam)
/* Get various dump options (preferences) from the user. */
{
	LONG retcode = 0;
	
	switch (msg) {
	case WM_INITDIALOG:
		{
			LPSHAREDWIZDATA pdata;
			
			/*
				Get the shared data from PROPSHEETPAGE lparam value and load it
				into GWL_USERDATA.
			*/
			pdata = (LPSHAREDWIZDATA) ((LPPROPSHEETPAGE) lparam)->lParam;
			SetWindowLong(win, GWL_USERDATA, (long) pdata);
			SetWindowFont(GetDlgItem(win, LBL_HEADING),
					pdata->heading_font, TRUE);
			/* set the `add drop statements' "on" by default */
			CheckDlgButton(win, CHK_ADDDROP, BST_CHECKED);
			retcode = 1;
		}
		break;
	case WM_NOTIFY:
		{
			LPNMHDR lpnm = (LPNMHDR) lparam;
			switch (lpnm->code) {
			case PSN_SETACTIVE:
				{
					HICON icon;
					
					PropSheet_SetWizButtons(GetParent(win),
							PSWIZB_BACK | PSWIZB_NEXT);
					icon = LoadIcon(myapp, MAKEINTRESOURCE(ICO_LOGO));
					SendMessage(lpnm->hwndFrom, WM_SETICON,
							(WPARAM) ICON_BIG, (LPARAM) icon);
					SendMessage(lpnm->hwndFrom, WM_SETICON,
							(WPARAM) ICON_SMALL, (LPARAM) icon);
					SetWindowText(GetDlgItem(GetParent(win), IDHELP),
							about_btntext);
					center_window(lpnm->hwndFrom);
				}
				break;
			case PSN_HELP:		/* show About box */
				DialogBox(myapp, MAKEINTRESOURCE(DLG_ABOUT), lpnm->hwndFrom,
						(DLGPROC) about_box);
				break;
			case PSN_WIZNEXT:	/* handle a next button click here */
				adddrop = IsButtonState(win, CHK_ADDDROP, BST_CHECKED);
				dumpdefn = IsButtonState(win, CHK_NOTABLES, BST_UNCHECKED);
				dumpdata = IsButtonState(win, CHK_NODATA, BST_UNCHECKED);
				dumpuser = IsButtonState(win, CHK_NOLOGINS, BST_UNCHECKED);
				dumpviews = IsButtonState(win, CHK_NOVIEWS, BST_UNCHECKED);
				dumpprocs = IsButtonState(win, CHK_NOPROCS, BST_UNCHECKED);
				dumpudts = IsButtonState(win, CHK_NOTYPES, BST_UNCHECKED);
				break;
			case PSN_RESET:		/* handle a cancel button click here */
				cancelled = 1;
				break;
			case PSN_QUERYCANCEL: /* confirm quit */
				{
					int rc = MessageBox(win,
							"You have cancelled the operation.\n"
							"Are you sure you wish to quit?", progname,
							MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON2);
					if (rc == IDNO)
						SetWindowLong(win, DWL_MSGRESULT, (long) TRUE);
					else
						SetWindowLong(win, DWL_MSGRESULT, (long) FALSE);
					retcode = 1;
				}
				break;
			default:
				break;
			}
			break;
		}
	default:
		break;
	}
	UNREFERENCED_PARAMETER(wparam);
	return retcode;
}

LRESULT CALLBACK get_output_file(HWND win, UINT msg,
		WPARAM wparam, LPARAM lparam)
/* Get the output file name from the user. */
{
	LONG retcode = 0;
	
	switch (msg) {
	case WM_INITDIALOG:
		{
			LPSHAREDWIZDATA pdata;
			
			/*
				Get the shared data from PROPSHEETPAGE lparam value and load it
				into GWL_USERDATA.
			*/
			pdata = (LPSHAREDWIZDATA) ((LPPROPSHEETPAGE) lparam)->lParam;
			SetWindowLong(win, GWL_USERDATA, (long) pdata);
			SetWindowFont(GetDlgItem(win, LBL_HEADING),
					pdata->heading_font, TRUE);
			retcode = 1;
		}
		break;
	case WM_COMMAND:
		switch (LOWORD(wparam)) {
		case TXT_FILENAME:		/* output file name */
			if (HIWORD(wparam) == EN_CHANGE) {
				if (Edit_GetTextLength(GetDlgItem(win, TXT_FILENAME)) > 0)
					PropSheet_SetWizButtons(GetParent(win),
							PSWIZB_BACK | PSWIZB_NEXT);
				else
					PropSheet_SetWizButtons(GetParent(win), PSWIZB_BACK);
			}
			break;
		case BTN_BROWSE:		/* browse button */
			{
				OPENFILENAME ofn = {0};
				
				/* fill open dialog box structure */
				ofn.lStructSize = sizeof(OPENFILENAME);
				ofn.hwndOwner = 0;
				ofn.lpstrFilter =
						"SQL Files (*.sql)\0*.sql\0All Files (*.*)\0*.*\0";
				ofn.nFilterIndex = 1;
				output_file[0] = 0;	/* no initialization needed */
				ofn.lpstrFile = output_file;
				ofn.nMaxFile = BUFSIZ;
				ofn.lpstrFileTitle = 0;
				ofn.lpstrInitialDir = 0;
				ofn.lpstrTitle = 0;
				ofn.Flags = OFN_HIDEREADONLY | OFN_NOREADONLYRETURN
						| OFN_PATHMUSTEXIST;
				ofn.lpstrDefExt = "sql";
				/* ...and show up the dialog box */
				if (GetSaveFileName(&ofn))
					SetWindowText(GetDlgItem(win, TXT_FILENAME), output_file);
			}
			break;
		default:
			break;
		}
		break;
	case WM_NOTIFY:
		{
			LPNMHDR lpnm;
			
			lpnm = (LPNMHDR) lparam;
			switch (lpnm->code) {
			case PSN_SETACTIVE:
				{
					HICON icon;
					
					if (Edit_GetTextLength(GetDlgItem(win, TXT_FILENAME)) > 0)
						PropSheet_SetWizButtons(GetParent(win),
								PSWIZB_BACK | PSWIZB_NEXT);
					else
						PropSheet_SetWizButtons(GetParent(win), PSWIZB_BACK);
					icon = LoadIcon(myapp, MAKEINTRESOURCE(ICO_LOGO));
					SendMessage(lpnm->hwndFrom, WM_SETICON,
							(WPARAM) ICON_BIG, (LPARAM) icon);
					SendMessage(lpnm->hwndFrom, WM_SETICON,
							(WPARAM) ICON_SMALL, (LPARAM) icon);
					SetWindowText(GetDlgItem(GetParent(win), IDHELP),
							about_btntext);
					center_window(lpnm->hwndFrom);
				}
				break;
			case PSN_HELP:		/* show About box */
				DialogBox(myapp, MAKEINTRESOURCE(DLG_ABOUT), lpnm->hwndFrom,
						(DLGPROC) about_box);
				break;
			case PSN_WIZNEXT:	/* handle a next button click here */
				{
					char emsg[BUFSIZ];
					bool ovwrite;
					bool create = true; /* ok to create the file? */
					
					ovwrite = IsButtonState(win, CHK_OVRWRITE, BST_CHECKED);
					Edit_GetText(GetDlgItem(win, TXT_FILENAME), output_file,
							BUFSIZ);
					if (!ovwrite) {
						bool fexists = exists(output_file);
						if (fexists) {
							int rc;
							
							sprintf(emsg, "The file '%s' already exists. "
									"Do you want to overwrite it?",
									output_file);
							rc = MessageBox(0, emsg, progname,
									MB_YESNO|MB_DEFBUTTON2|MB_ICONQUESTION);
							if (rc == IDYES)
								create = true;
							else
								create = false;
						}
						else
							create = true;
					}
					if (create) {
						if ((ofp = fopen(output_file, "w")) == 0) {
							sprintf(emsg, "An error occured while "
									"attempting to create file '%s'. "
									"Please specify another.", output_file);
							MessageBox(0, emsg, progname,
									MB_OK | MB_ICONINFORMATION);
							SetWindowLong(win, DWL_MSGRESULT, -1L);
							retcode = 1;
							break;
						}
						else {	/* ok to jump to finish page */
							SetWindowLong(win, DWL_MSGRESULT, FALSE);
							retcode = 1;
						}
					}
					else {		/* don't proceed! */
						SetWindowLong(win, DWL_MSGRESULT, -1L);
						retcode = 1;
						break;
					}
				}
				break;
			case PSN_RESET:		/* handle a cancel button click here */
				cancelled = 1;
				break;
			case PSN_QUERYCANCEL: /* confirm quit */
				{
					int rc = MessageBox(win,
							"You have cancelled the operation.\n"
							"Are you sure you wish to quit?", progname,
							MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON2);
					if (rc == IDNO)
						SetWindowLong(win, DWL_MSGRESULT, (long) TRUE);
					else
						SetWindowLong(win, DWL_MSGRESULT, (long) FALSE);
					retcode = 1;
				}
				break;
			default:
				break;
			}
			break;
		}
	default:
		break;
	}
	return retcode;
}

LRESULT CALLBACK confirm(HWND win, UINT msg, WPARAM wparam, LPARAM lparam)
/*
	Show the finish dialog of the wizard and confirm settings with the user.
	This screen may be replaced with a screen summarizing the connection
	settings and preferences chosen by the user.
*/
{
	LONG retcode = 0;
	
	switch (msg) {
	case WM_INITDIALOG:
		{
			LPSHAREDWIZDATA pdata;
			
			/*
				Get the shared data from PROPSHEETPAGE lparam value and load it
				into GWL_USERDATA.
			*/
			pdata = (LPSHAREDWIZDATA) ((LPPROPSHEETPAGE) lparam)->lParam;
			SetWindowLong(win, GWL_USERDATA, (long) pdata);
			SetWindowFont(GetDlgItem(win, LBL_CAPTION),
					pdata->title_font, TRUE);
			retcode = 1;
		}
		break;
	case WM_NOTIFY:
		{
			LPNMHDR lpnm = (LPNMHDR) lparam;
			switch (lpnm->code) {
			case PSN_SETACTIVE:
				{
					HICON icon;
					
					PropSheet_SetWizButtons(GetParent(win),
							PSWIZB_BACK | PSWIZB_FINISH);
					icon = LoadIcon(myapp, MAKEINTRESOURCE(ICO_LOGO));
					SendMessage(lpnm->hwndFrom, WM_SETICON,
							(WPARAM) ICON_BIG, (LPARAM) icon);
					SendMessage(lpnm->hwndFrom, WM_SETICON,
							(WPARAM) ICON_SMALL, (LPARAM) icon);
					SetWindowText(GetDlgItem(GetParent(win), IDHELP),
							about_btntext);
					center_window(lpnm->hwndFrom);
				}
				break;
			case PSN_HELP:		/* show About box */
				DialogBox(myapp, MAKEINTRESOURCE(DLG_ABOUT), lpnm->hwndFrom,
						(DLGPROC) about_box);
				break;
			case PSN_WIZNEXT:	/* handle a next button click here */
				break;
			case PSN_RESET:		/* handle a cancel button click here */
				cancelled = 1;
				break;
			case PSN_QUERYCANCEL: /* confirm quit */
				{
					int rc = MessageBox(win,
							"You have cancelled the operation.\n"
							"Are you sure you wish to quit?", progname,
							MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON2);
					if (rc == IDNO)
						SetWindowLong(win, DWL_MSGRESULT, (long) TRUE);
					else
						SetWindowLong(win, DWL_MSGRESULT, (long) FALSE);
					retcode = 1;
				}
				break;
			case PSN_WIZFINISH:	/* handle a finish button click here */
				break;
			default:
				break;
			}
		}
		break;
	default:
		break;
	}
	UNREFERENCED_PARAMETER(wparam);
	return retcode;
}

void show_wizard()
/* Start the wizard. */
{
	LOGFONT log_font;
	HDC dc;
	char pg = 0;				/* page # of the wizard */
	char* fontname = "Verdana";	/* wizard title font */
	int tfontsz = 12;			/* wizard title font size */
	int hfontsz = 8;			/* wizard heading font size */
	NONCLIENTMETRICS ncm = {0};
	PROPSHEETPAGE psp = {0};	/* defines the property sheet pages */
	HPROPSHEETPAGE ahpsp[LASTPAGE+1] = {0};
	PROPSHEETHEADER psh = {0};	/* defines the property sheet */
	SHAREDWIZDATA wizdata = {0};

	/* load About SqlDump button text */
	LoadString(myapp, LBL_ABOUT, about_btntext, sizeof(about_btntext));
	psp.dwSize = sizeof(psp);	/* set up the introduction screen */
	psp.dwFlags = PSP_DEFAULT | PSP_USEICONID | PSP_HASHELP;
	psp.hInstance = myapp;
	psp.pszIcon = MAKEINTRESOURCE(ICO_LOGO);
	psp.lParam = (LPARAM) &wizdata;
	psp.pszTemplate = MAKEINTRESOURCE(DLG_WELCOME);
	psp.pfnDlgProc = (DLGPROC) show_intro;
	ahpsp[pg] = CreatePropertySheetPage(&psp);
	psp.dwSize = sizeof(psp);	/* set up the server settings screen */
	psp.dwFlags = PSP_DEFAULT | PSP_USEICONID | PSP_HASHELP;
	psp.hInstance = myapp;
	psp.pszIcon = MAKEINTRESOURCE(ICO_LOGO);
	psp.lParam = (LPARAM) &wizdata;
	psp.pszTemplate = MAKEINTRESOURCE(DLG_SERVEROPT);
	psp.pfnDlgProc = (DLGPROC) get_connection_settings;
	ahpsp[++pg] = CreatePropertySheetPage(&psp);
	psp.dwSize = sizeof(psp);	/* set up the dump options page */
	psp.dwFlags = PSP_DEFAULT | PSP_USEICONID | PSP_HASHELP;
	psp.hInstance = myapp;
	psp.pszIcon = MAKEINTRESOURCE(ICO_LOGO);
	psp.lParam = (LPARAM) &wizdata;
	psp.pszTemplate = MAKEINTRESOURCE(DLG_OPTIONS);
	psp.pfnDlgProc = (DLGPROC) show_options;
	ahpsp[++pg] = CreatePropertySheetPage(&psp);
	psp.dwSize = sizeof(psp);	/* set up the dump output page */
	psp.dwFlags = PSP_DEFAULT | PSP_USEICONID | PSP_HASHELP;
	psp.hInstance = myapp;
	psp.pszIcon = MAKEINTRESOURCE(ICO_LOGO);
	psp.lParam = (LPARAM) &wizdata;
	psp.pszTemplate = MAKEINTRESOURCE(DLG_OUTPUTFILE);
	psp.pfnDlgProc = (DLGPROC) get_output_file;
	ahpsp[++pg] = CreatePropertySheetPage(&psp);
	psp.dwSize = sizeof(psp);	/* set up the summary page */
	psp.dwFlags = PSP_DEFAULT | PSP_USEICONID | PSP_HASHELP;
	psp.hInstance = myapp;
	psp.pszIcon = MAKEINTRESOURCE(ICO_LOGO);
	psp.lParam = (LPARAM) &wizdata;
	psp.pszTemplate = MAKEINTRESOURCE(DLG_FINISH);
	psp.pfnDlgProc = (DLGPROC) confirm;
	ahpsp[++pg] = CreatePropertySheetPage(&psp);
	psh.dwSize = sizeof(psh);	/* create the wizard */
	psh.hInstance = myapp;
	psh.hwndParent = 0;
	psh.phpage = ahpsp;
	psh.dwFlags = PSH_DEFAULT | PSH_WIZARD;
	psh.nStartPage = 0;
	psh.nPages = LASTPAGE;

	/* set up title font for intro/end screens */
	ncm.cbSize = sizeof(ncm);
	SystemParametersInfo(SPI_GETNONCLIENTMETRICS, 0, &ncm, 0);
	log_font = ncm.lfMessageFont;
	log_font.lfWeight = FW_BOLD;
	lstrcpy(log_font.lfFaceName, fontname);
	dc = GetDC(0);				/* get screen DC */
	log_font.lfHeight = 0 - GetDeviceCaps(dc, LOGPIXELSY)*tfontsz/72;
	wizdata.title_font = CreateFontIndirect(&log_font);
	ReleaseDC(0, dc);
	/* set up heading font for interior screens */
	ncm.cbSize = sizeof(ncm);
	SystemParametersInfo(SPI_GETNONCLIENTMETRICS, 0, &ncm, 0);
	log_font = ncm.lfMessageFont;
	log_font.lfWeight = FW_BOLD;
	lstrcpy(log_font.lfFaceName, fontname);
	dc = GetDC(0);				/* get screen DC */
	log_font.lfHeight = 0 - GetDeviceCaps(dc, LOGPIXELSY)*hfontsz/72;
	wizdata.heading_font = CreateFontIndirect(&log_font);
	ReleaseDC(0, dc);

	PropertySheet(&psh);		/* show the wizard */
	DeleteObject(wizdata.title_font);
	DeleteObject(wizdata.heading_font);
}
