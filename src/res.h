/*
	Copyright (C) 2001-2002 Gopalarathnam V. <gopal@developercentral.org>.
	
	This file is a part of SqlDump.
	
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with SqlDump; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <windows.h>

#ifndef _RES_H
#define _RES_H

#ifndef IDC_STATIC				/* gnu doesn't define IDC_STATIC! */
#define IDC_STATIC         -1
#endif

/* Common user-interface elements */
#define ICO_LOGO           1001
#define ICO_WARNING        1002
#define ICO_INFORMATION    1003

#define LBL_ABOUT          1201
#define LBL_CAPTION        1202
#define LBL_HEADING        1203
#define LBL_DESC           1204
#define LBL_NOTICE         1205
#define PIC_NOTICE         1206

#define STR_ICCFAIL        1301
#define STR_ABORTMSG       1302
#define STR_ESERVER        1303
#define STR_ECREALOG       1304
#define STR_EEXISTS        1305
#define STR_ECREAFILE      1306
#define STR_ETHREAD        1307
#define STR_ENOVIEWER      1308

/* About box */
#define DLG_ABOUT          1401
#define LBL_PROGNAME       1402
#define LBL_BUILDOPT       1403

/* Welcome screen */
#define DLG_WELCOME        1501

/* Server options screen */
#define DLG_SERVEROPT      1601
#define LBL_HOST           1602
#define CBO_HOST           1603
#define LBL_USER           1604
#define TXT_USER           1605
#define LBL_PASSWD         1606
#define TXT_PASSWD         1607
#define LBL_DB             1608
#define CBO_DB             1609
#define BTN_REFRESH        1610

/* Options screen */
#define DLG_OPTIONS        1701
#define CHK_ADDDROP        1702
#define CHK_NOTABLES       1703
#define CHK_NODATA         1704
#define CHK_NOLOGINS       1705
#define CHK_NOVIEWS        1706
#define CHK_NOPROCS        1707
#define CHK_NOTYPES        1708

/* Output file screen */
#define DLG_OUTPUTFILE     1801
#define LBL_FILENAME       1802
#define TXT_FILENAME       1803
#define BTN_BROWSE         1804
#define CHK_OVRWRITE       1805

/* Completion screen */
#define DLG_FINISH         1901

/* Progress window */
#define DLG_PROGRESS       2001
#define STR_STATUSLBL      2002
#define STR_DISMISSBTN     2003
#define LBL_METER          2004
#define PBC_METER          2005
#define LBL_STATUS         2006
#define LVW_STATUS         2007
#define BTN_CANCEL         2008
#define BTN_VIEWLOG        2009
#define BTN_OUTPUT         2010

/* Task labels, widths */
#define LBL_TASKNAME       3001
#define LBL_TASKSTATUS     3002
#define LBL_TASKREASON     3003
#define TASKNAME_WIDTH      160
#define TASKSTATUS_WIDTH     60
#define TASKREASON_WIDTH    196

#endif /* whole file */
