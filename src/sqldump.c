/*
	Copyright (C) 2001-2002 Gopalarathnam V. <gopal@developercentral.org>.
	
	This file is a part of SqlDump.
	
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with SqlDump; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "sqlwrap.h"
#include "sqldump.h"

char* progname;					/* program name */
MSSQL* mssql = 0;				/* global connection identifier */
char log_file[MAX_PATH];		/* log file name */

static struct option opttab[] = { /* long options */
	{ "version", 0, 0, 'V' },
	{ "force", 0, 0, 'F' },
	{ "host", 1, 0, 'h' },
	{ "user", 1, 0, 'u' },
	{ "passwd", 1, 0, 'p' },
	{ "add-drop", 0, 0, 'D' },
	{ "no-defn", 0, 0, 't' },
	{ "no-data", 0, 0, 'd' },
	{ "cond", 1, 0, 'w' },
	{ "no-logins", 0, 0, 'g' },
	{ "no-procs", 0, 0, 's' },
	{ "no-views", 0, 0, 'v' },
	{ "output-file", 1, 0, 'o' },
	{ "list-servers", 0, 0, 'l' },
	{ "no-udts", 0, 0, 'y' },
	{ 0, 0, 0, 0 } 				/* terminal element */
};

int main(int argc, char* argv[])
/* Dump a Microsoft SQL Server database. */
{
	char* host;
	char* user;
	char* passwd;
	char* db;
	char* ofile = 0;			/* output file */
	FILE* ofp = stdout;			/* default output stream */
	bool ovwrite = false;		/* force overwriting existing file */
	int c, optindex;

	/* initialize program variables. */
	progname = strdup("sqldump");
	opterr = 1; 				/* let getopt announce the error! */
	host = user = passwd = 0;
	allok = true;				/* assume everything will be successful */
	setbuf(stdout, 0);			/* disable output buffering */
	setbuf(stderr, 0);

	while ((c = getopt_long(argc, argv,
					"VFh:u:p:Dtdw:gsvlo:y",
					(const option*) &opttab, &optindex)) != EOF) {
		if (c == 0)				/* long option */
			c = opttab[optindex].val;
		switch (c) {
		case 'V':
			version_info();
			break;
		case 'F':
			ovwrite = true;
			break;
		case 'h':
			host = optarg;
			break;
		case 'u':
			user = optarg;
			break;
		case 'p':
			passwd = optarg;
			break;
		case 'D':
			adddrop = true;
			break;
		case 't':
			dumpdefn = false;
			break;
		case 'd':
			dumpdata = false;
			break;
		case 'w':
			where = optarg;
			break;
		case 'g':
			dumpuser = false;
			break;
		case 's':
			dumpprocs = false;
			break;
		case 'v':
			dumpviews = false;
			break;
		case 'l':
			listserv = true;
			break;
		case 'o':
			ofile = optarg;
			break;
		case 'y':
			dumpudts = false;
			break;
		default:				/* option unrecognized */
			usage_exit();
		}
	}
	
	if (listserv) {				/* list sql servers and quit */
		char** servers;
		int i, n;
		
		if ((n = mssql_list_servers(&servers)) > 0) {
			for (i = 0; i < n; ++i) {
				printf("%s\n", servers[i]);
				free(servers[i]);
			}
		}
		exit(0);				/* normal exit! */
	}
	if (optind >= argc) usage_exit();
	if (!dumpdefn && !dumpdata && !dumpuser && !dumpprocs && !dumpviews) {
		fprintf(stderr, "%s: nothing to dump!\n", progname);
		usage_exit();
	}
	if (!passwd) passwd = getpw(0);
	if (!passwd) exit(0);		/* seems like the user wants to quit */

	if ((lf = create_log(argv[optind])) == 0)
		fprintf(stderr, "%s: can't create log file %s\n", argv[optind]);

	if ((db = (char*) malloc(BUFSIZ)) == 0) {
		fprintf(stderr, "%s: out of memory\n", progname);
		exit(1);
	}
	escape(db, argv[optind++]);
	if (ofile) {				/* if output file specified */
		bool create = true;		/* ok to create the file? */
		
		if (!ovwrite) {
			bool fexists = exists(ofile);
			if (fexists) {
				char c;
				
				printf("The file '%s' already exists. Overwrite? ",
						ofile);
				c = getchar();
				if (c == 'y' || c == 'Y')
					create = true;
				else
					create = false;
			}
		}

		if (create) {			/* yes! go ahead create the file */
			if ((ofp = fopen(ofile, "w")) == 0) {
				fprintf(stderr, "%s: can't create %s\n", ofile);
				exit(1);
			}
		}
		else {					/* abort! */
			fprintf(stderr, "%s: will not dump to /dev/null!\n", progname);
			exit(1);
		}
	}
	/* connect to Microsoft SQL Server */
	if ((mssql = serverconnect(host, user, passwd, db)) == 0) {
		fprintf(stderr, "%s: can't connect to sql server: %s\n",
				progname, get_last_error());
		exit(1);
	}
	write_header(progname, mssql, ofp);	/* display startup banner */
	write_log(lf, "About to export specified database ...\n");
	if (dumpuser) {				/* dump database users */
		if (dump_users(mssql, ofp) < 0) {
			fprintf(stderr, "%s: can't retrieve users in '%s': %s\n",
					progname, db, get_last_error());
			allok = false;
		}
	}
	if (dumpudts) {				/* dump user-defined types */
		if (dump_udts(mssql, ofp) < 0) {
			fprintf(stderr, "%s: can't retrieve datatypes in '%s': %s\n",
					progname, db, get_last_error());
			allok = false;
		}
	}
	if (argc > optind) {		/* dump tables and/or data */
		/*
			Build the list of database tables. This is needed for dumping table
			definition followed by table data to avoid problems while dumping
			foreign key constraints.
		*/
		struct tab* p;
		
		for (; optind < argc; ++optind) {
			int n;
			
			if ((n = get_table_structure(mssql, argv[optind], ofp)) < 0) {
				fprintf(stderr, "%s: can't get info about table '%s': %s\n",
						progname, argv[optind], get_last_error());
				write_log(lf, "%8ld   rows exported\n", 0);
				allok = false;
			}
			else {
				if (!install(argv[optind], n))
					fprintf(stderr, "%s: can't install node ('%s', %d)\n",
							progname, argv[optind], n);
			}
		}

		for (p = tablist; p; p = p->link) /* dump constraints */
			fprintf(stderr, "debug: dump constraints (%s,%d)\n",
					p->name, p->cols);
		for (p = tablist; p; p = p->link) /* dump relationships */
			fprintf(stderr, "debug: dump relationships (%s,%d)\n",
					p->name, p->cols);
		if (dumpdata)
			for (p = tablist; p; p = p->link) /* dump table data */
				dump_table(mssql, p->name, p->cols, ofp);
		goto end;				/* bypass procs, views */
	}
	else {
		/*
			Build the list of database tables. This is needed for dumping table
			definition followed by table data to avoid problems while dumping
			foreign key constraints.
		*/
		struct tab* p;
		char* tmpname;
		
		while ((tmpname = get_table_name(mssql)) != 0) {
			int n;
			
			if ((n = get_table_structure(mssql, tmpname, ofp)) < 0) {
				fprintf(stderr, "%s: can't get info about table '%s': %s\n",
						progname, tmpname, get_last_error());
				allok = false;
				write_log(lf, "%8ld   rows exported\n", 0);
			}
			else {
				if (!install(tmpname, n))
					fprintf(stderr, "%s: can't install node ('%s', %d)\n",
							progname, tmpname, n);
			}
		}

		for (p = tablist; p; p = p->link) /* dump constraints */
			fprintf(stderr, "debug: dump constraints (%s,%d)\n",
					p->name, p->cols);
		for (p = tablist; p; p = p->link) /* dump relationships */
			fprintf(stderr, "debug: dump relationships (%s,%d)\n",
					p->name, p->cols);
		if (dumpdata)
			for (p = tablist; p; p = p->link) /* dump table data */
				dump_table(mssql, p->name, p->cols, ofp);
	}
	if (dumpviews) {			/* dump views */
		char* temp;
		bool ok = true;			/* dump view was successful */
		
		fprintf(ofp, "\n-- Dumping views in '%s'\n--\n", db);
		while ((temp = get_view_name(mssql)) != 0) {
			write_log(lf, ". exporting view %32s", temp);
			if (dump_view(mssql, temp, ofp) < 0) {
				fprintf(stderr, "%s: can't dump view '%s': %s\n",
						progname, temp, get_last_error());
				allok = ok = false;
			}
			write_log(lf, "%15s exported\n", (ok ? "" : "not"));
		}
	}
	if (dumpprocs) {			/* dump stored procedures */
		char* temp;
		bool ok = true;			/* dump proc was successful */
		
		fprintf(ofp, "\n-- Dumping procedures in '%s'\n--\n", db);
		while ((temp = get_proc_name(mssql)) != 0) {
			write_log(lf, ". exporting procedure %27s", temp);
			if (dump_proc(mssql, temp, ofp) < 0) {
				fprintf(stderr, "%s: can't dump proc '%s': %s\n",
						progname, temp, get_last_error());
				allok = ok = false;
			}
			write_log(lf, "%15s exported\n", (ok ? "" : "not"));
		}
	}
end:
	freetablist();
	free(db);
	write_log(lf, "Export terminated %s\n",
			(allok ? "successfully without warnings." : "with warnings."));
	close_log(lf);
	serverdisconnect(mssql);	/* terminate connection with server */
	return 0;					/* thats all, folks! */
}

void usage_exit()
/* Print usage information and exit to the Operating System. */ 
{
	printf("Usage: %s [OPTION]... DATABASE [TABLE]...\n"
			"Dumping definitions and data for an SQL Server database.\n"
			"Options:\n"
			"  -V, --version"
			"                   Show version information and exit\n"
			"  -F, --force"
			"                     Force overwriting of existing file\n"
			"  -h server, --host=server"
			"        Connect to server.\n"
			"  -u login, --user=login"
			"          User to log on as (if not the default sa.)\n"
			"  -p password, --passwd=password"
			"  Password to use when connecting to server\n"
			"  -D, --add-drop"
			"                  Add a 'drop table' before each create.\n"
			"  -t, --no-defn"
			"                   Don't write table creation info.\n"
			"  -d, --no-data"
			"                   Don't dump table data.\n"
			"  -w cond, --where=cond"
			"           Dump only select records.\n"
			"  -g, --no-logins"
			"                 Don't dump logins.\n"
			"  -s, --no-procs"
			"                  Don't dump stored procedures.\n"
			"  -v, --no-views"
			"                  Don't dump views.\n"
			"  -l, --list-servers"
			"              List registered SQL Servers and exit.\n"
			"  -o file, --output-file=file"
			"     Dump output to file instead of stdout.\n"
			"  -y, --no-udts"
			"                   Don't dump user-defined datatypes.\n",
			progname);
	exit(0);
}

void version_info()
/* Display SqlDump version information and exit to the Operating System. */
{
	printf("%s Ver %s (libmssql version %s)\n"
			"Copyright (C) 2001-2002 "
			"Gopalarathnam V. <gopal@developercentral.org>.\n"
			"Build options: %s\n\n"
			"This program is free software; you can redistribute it and/or\n"
			"modify it under the terms of the GNU General Public License\n"
			"as published by the Free Software Foundation; either version 2\n"
			"of the License, or (at your option) any later version.\n\n"
			"This program is distributed in the hope that it will be useful,\n"
			"but WITH ANY WARRANTY; without even the implied warranty of\n"
			"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
			"GNU General Public License for more details.\n\n"
			"You should have received a copy of the GNU General Public "
			"License\nalong with this program; if not, write to the Free "
			"Software\nFoundation, Inc., "
			"59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.\n\n",
			progname, DUMP_VERSION, LIB_VERSION, CONFIG_COMMAND);
	exit(0);
}
