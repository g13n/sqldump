/*
	Copyright (C) 2001-2002 Gopalarathnam V. <gopal@developercentral.org>.
	
	This file is a part of SqlDump.
	
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with SqlDump; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>
#include <windows.h>
#include <windowsx.h>
#include "mssql.h"
#include "config.h"

/* global constants and prototypes get defined here */
#ifndef _SQLDUMPW_H
#define _SQLDUMPW_H

/* gui.c */
extern char cancelled;
extern void center_window(HWND win);
extern bool load_dblist(char* host, char* user, char* passwd, HWND cwin);
extern LRESULT CALLBACK about_box(HWND win, UINT msg,
		WPARAM wparam, LPARAM lparam);
extern void show_wizard();

/* sqldumpw.c */
extern HINSTANCE myapp;
extern MSSQL* mssql;
extern ulong dbsize;
extern char* progname;
extern FILE* ofp;
extern LRESULT CALLBACK show_progress(HWND win, UINT msg,
		WPARAM wparam, LPARAM lparam);
extern DWORD WINAPI progress_thread(LPVOID dlg_handle);

#endif /* whole file */
