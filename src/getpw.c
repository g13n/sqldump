/*
	Copyright (C) 2001-2002 Gopalarathnam V. <gopal@developercentral.org>.
	
	This file is a part of SqlDump.
	
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with SqlDump; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <conio.h>
#include <string.h>
#include "sqldump.h"

#define MAXPWSIZE 64			/* maximum size for password input */

#ifdef __GNUC__
#define cputs _cputs			/* gnu doesn't have cputs */
#endif

char *getpw(const char* prompt)
/* Read password from terminal and return it. */
{
	int c, n;
	char buf[MAXPWSIZE];
	char pass_prompt[MAXPWSIZE];
	char* bufp = buf;
	
	if (!prompt)
		strcpy(pass_prompt, "Password:");
	else
		strncpy(pass_prompt, prompt, MAXPWSIZE);
	cputs(pass_prompt);			/* output prompt */
	for (n = 0; n < MAXPWSIZE; n++) { /* overflow check */
		c = _getch();			/* read a character from terminal */
		/*
			If this is a backspace character, back off and erase the previous
			character. Watchout for the beginning of the buffer.
		*/
		if (c == '\b') {
			if (bufp != buf) --bufp;
			continue;
		}
		if (c == '\x3') return 0; /* user break! */
		/* if this is a newline break out of the loop */
		if (c == '\n' || c == '\r') break;
		/* ...or copy the character to the buffer */
		*bufp++ = (char) c;
	}
	/*
		Terminate buffer and output a newline assuming the user would have
		hit an enter.
	*/
	*bufp = 0;
	while ((c = getchar()) != '\n')
		;						/* swallow chars till end of line */
	return strdup(buf);			/* caller's responsibility to free buffer */
}
