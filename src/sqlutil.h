/*
	Copyright (C) 2001-2002 Gopalarathnam V. <gopal@developercentral.org>.
	
	This file is a part of SqlDump.
	
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with SqlDump; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "mssql.h"		   /* definitions from the libmssql library */

#ifndef _SQLUTIL_H
#define _SQLUTIL_H

/*
	The list of Microsoft SQL Server data types.
	This list may be obtained automatically from Microsoft SQL Server in the
	next release.
*/
struct sqltypes {
	char *name;					/* data type name */
	bool prec;					/* must specify precision/length */
	bool scale;					/* must specify scale */
};

struct tab {
	char* name;					/* table name */
	uint cols;					/* count of number of columns */
	struct tab* link;			/* link to next chain */
};
extern struct tab* tablist;		/* list of tables in current database */

extern char* quote(char*, const char*);
extern char* firstword(const char*);
extern int strlcmp(const char*, const char*);
extern struct sqltypes* type_lookup(const char*);
extern bool is_keyword(const char*);
extern char* escape(char*, const char*);
extern bool exists(const char*);
extern bool install(const char*, uint);
extern void freetablist();

#endif /* whole file */
