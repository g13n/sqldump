/*
	Copyright (C) 2001-2002 Gopalarathnam V. <gopal@developercentral.org>.
	
	This file is a part of SqlDump.
	
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with SqlDump; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "sqldumpw.h"
#include <math.h>
#include <commctrl.h>
#include <shellapi.h>
#include "sqlwrap.h"
#include "res.h"

#define addnewitem(lv, i, item, text) \
	item.mask = LVIF_TEXT; \
	item.iItem = (i); \
	item.iSubItem = 0; \
	item.pszText = text; \
	ListView_InsertItem((lv), &item)
#define doprogress(dlg, pc) SendDlgItemMessage((dlg), (pc), PBM_STEPIT, 0, 0)

HINSTANCE myapp;				/* application instance */
MSSQL* mssql = 0;				/* global connection identifier */
ulong dbsize = 0L;				/* database size in kilobytes */
char* progname;					/* program name */
FILE* ofp;						/* dump output file */
bool done = false;				/* is dump complete? */
char output_file[MAX_PATH];		/* output file name */
char log_file[MAX_PATH];		/* log file name */

int WINAPI WinMain(HINSTANCE app, HINSTANCE prevapp, LPSTR args, int show)
{
	INITCOMMONCONTROLSEX iccex;
	MSG msg;
	HWND win;
	DWORD thread_id;
	HANDLE thread;

	progname = strdup("SqlDump Database Dump Wizard");

	/* Attempt to load & initialize the common control library. */
	iccex.dwSize = sizeof(INITCOMMONCONTROLSEX);
	iccex.dwICC = ICC_LISTVIEW_CLASSES | ICC_PROGRESS_CLASS;
	if (!InitCommonControlsEx(&iccex)) {
		MessageBox(0, "Failed to load and initialize common control classes. "
				"Make sure you have the latest version of the common control "
				"library. You may download the latest common control library "
				"from http://www.microsoft.com/msdownload/ieplatform/ie/comctrlx86.asp",
				progname, MB_OK | MB_ICONSTOP);
		ExitProcess(1);
	}
	
	allok = true;				/* assume everything will be successful */
	myapp = app;				/* set global application instance */
	show_wizard();				/* run the wizard */
	if (cancelled) ExitProcess(1); /* user has cancelled the operation! */

	/*
		Now the progress dialog is being created where the actual process
		is done.
	*/
	win = CreateDialog(app, MAKEINTRESOURCE(DLG_PROGRESS),
		0, (DLGPROC) show_progress);
	/* create a thread to update the progress window */
	thread = CreateThread(0, 0, progress_thread, win, 0, &thread_id);
	if (!thread) {				/* disaster! */
		MessageBox(0, "FATAL: can't start a new thread. "
				"System might be running in dangerously low resources. "
				"Aborting.", progname, MB_OK | MB_ICONSTOP);
		ExitProcess(1);
	}
	CloseHandle(thread);
	
	while (GetMessage(&msg, 0, 0, 0)) {
		if (!IsDialogMessage(win, &msg)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
	serverdisconnect(mssql);	/* close connection with sql server */
	
	UNREFERENCED_PARAMETER(prevapp);
	UNREFERENCED_PARAMETER(args);
	UNREFERENCED_PARAMETER(show);
	return 0;					/* normal shutdown */
}

LRESULT CALLBACK show_progress(HWND win, UINT msg,
		WPARAM wparam, LPARAM lparam)
/* Progress window callback procedure. */
{
	LRESULT retcode = 0;
	
	switch (msg) {
	case WM_INITDIALOG:
		{
			HICON icon;
			HWND cwin;
			LVCOLUMN col;
			char temp[BUFSIZ];
			int n;
			int metersz, normval = 1024;
			double meterstep;

			icon = LoadIcon(myapp, MAKEINTRESOURCE(ICO_LOGO));
			SendMessage(win, WM_SETICON, (WPARAM) ICON_BIG, (LPARAM) icon);
			SendMessage(win, WM_SETICON, (WPARAM) ICON_SMALL, (LPARAM) icon);
			cwin = GetDlgItem(win, LVW_STATUS);	/* get list view handle */
			col.mask = LVCF_TEXT | LVCF_WIDTH; /* add "task name" column */
			col.cx = TASKNAME_WIDTH;
			n = LoadString(myapp, LBL_TASKNAME, temp, BUFSIZ);
			col.pszText = strdup(temp);
			col.cchTextMax = n;
			ListView_InsertColumn(cwin, 0, &col);
			col.mask = LVCF_TEXT | LVCF_WIDTH; /* add status column */
			col.cx = TASKSTATUS_WIDTH;
			n = LoadString(myapp, LBL_TASKSTATUS, temp, BUFSIZ);
			col.pszText = strdup(temp);
			col.cchTextMax = n;
			ListView_InsertColumn(cwin, 1, &col);
			col.mask = LVCF_TEXT | LVCF_WIDTH; /* add reason column */
			col.cx = TASKREASON_WIDTH;
			n = LoadString(myapp, LBL_TASKREASON, temp, BUFSIZ);
			col.pszText = strdup(temp);
			col.cchTextMax = n;
			ListView_InsertColumn(cwin, 2, &col);
			metersz = dbsize;	/* compute progress meter length */
			meterstep = (double) dbsize / normval;
			SendDlgItemMessage(win, PBC_METER, PBM_SETRANGE,
					0, MAKELPARAM(0, metersz));
			SendDlgItemMessage(win, PBC_METER, PBM_SETSTEP,
					(long) ceil(meterstep), 0);
			center_window(win);
			SetFocus(GetDlgItem(win, BTN_CANCEL));
			EnableWindow(GetDlgItem(win, BTN_VIEWLOG), FALSE);
			EnableWindow(GetDlgItem(win, BTN_OUTPUT), FALSE);
			retcode = 1;
		}
		break;
	case WM_CLOSE:
		if (!done) {
			int rc = MessageBox(win,
					"You have cancelled the operation.\n"
					"Are you sure you wish to quit?", progname,
					MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON2);
			if (rc == IDNO) break;
		}
		DestroyWindow(win);
		break;
	case WM_COMMAND:
		switch (LOWORD(wparam)) {
		case BTN_CANCEL:
			SendMessage(win, WM_CLOSE, 0, 0);
			break;
		case BTN_VIEWLOG:		/* open log file using the default viewer */
			{
				int rc = (int) ShellExecute(win, "open", log_file,
						0, 0, SW_SHOW);
				if (rc < 33) {
					char cmdline[BUFSIZ];

					sprintf(cmdline, "notepad.exe %s", log_file);
					rc = WinExec(cmdline, SW_SHOW);
					if (rc < 32) {
						sprintf(cmdline, "No viewer could be located for "
								"opening file '%s'.", log_file);
						MessageBox(win, cmdline, progname,
								MB_OK | MB_ICONINFORMATION);
					}
				}
			}
			break;
		case BTN_OUTPUT:		/* open output sql using the default viewer */
			{
				int rc = (int) ShellExecute(win, "open", output_file,
						0, 0, SW_SHOW);
				if (rc < 33) {
					char cmdline[BUFSIZ];

					sprintf(cmdline, "notepad.exe %s", output_file);
					rc = WinExec(cmdline, SW_SHOW);
					if (rc < 32) {
						sprintf(cmdline, "No viewer could be located for "
								"opening file '%s'.", output_file);
						MessageBox(win, cmdline, progname,
								MB_OK | MB_ICONINFORMATION);
					}
				}
			}
			break;
		} 
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	UNREFERENCED_PARAMETER(lparam);
	return retcode;
}

DWORD WINAPI progress_thread(LPVOID dlg_handle)
/* Call various functions from the LibMssql library, dump script. */
{
	char* p;
	char temp[BUFSIZ];
	HWND win = (HWND) dlg_handle;
	HWND list = GetDlgItem(win, LVW_STATUS);
	LVITEM item;
	int nitems = 0;

	write_header(progname, mssql, ofp);
	write_log(lf, "About to export specified database ...\n");
	if (dumpuser) {				/* dump database users */
		addnewitem(list, nitems, item, "Dumping database users");
		if (dump_users(mssql, ofp) < 0) {
			ListView_SetItemText(list, nitems, 1, "Failed");
			ListView_SetItemText(list, nitems, 2, get_last_error());
			allok = false;
		}
		else
			ListView_SetItemText(list, nitems, 1, "Success");
		doprogress(win, PBC_METER);
		++nitems;
	}
	if (dumpudts) {				/* dump user-defined types */
		addnewitem(list, nitems, item, "Dumping user-defined types");
		if (dump_udts(mssql, ofp) < 0) {
			ListView_SetItemText(list, nitems, 1, "Failed");
			ListView_SetItemText(list, nitems, 2, get_last_error());
			allok = false;
		}
		else
			ListView_SetItemText(list, nitems, 1, "Success");
		doprogress(win, PBC_METER);
		++nitems;
	}
	while ((p = get_table_name(mssql)) != 0) { /* dump tables */
		int nr_fields, succeeded;
		
		sprintf(temp, "Dumping database table '%s'", p);
		addnewitem(list, nitems, item, temp);
		nr_fields = get_table_structure(mssql, p, ofp);
		doprogress(win, PBC_METER);
		succeeded = 1;
		if (nr_fields < 0)
			succeeded = 0;
		if (succeeded) {
			if (dumpdata)
				dump_table(mssql, p, nr_fields, ofp);
			ListView_SetItemText(list, nitems, 1, "Success");
		}
		else {
			ListView_SetItemText(list, nitems, 1, "Failed");
			ListView_SetItemText(list, nitems, 2, get_last_error());
			write_log(lf, "%8ld   rows exported\n", 0);
			allok = false;
		}
		doprogress(win, PBC_METER);
		++nitems;
	}
	if (dumpviews) {			/* dump views */
		bool ok = true;			/* dump view was successful */
		fprintf(ofp, "\n-- Dumping views\n--\n");
		while ((p = get_view_name(mssql)) != 0) {
			sprintf(temp, "Dumping database view '%s'", p);
			addnewitem(list, nitems, item, temp);
			write_log(lf, ". exporting view %32s", p);
			if (dump_view(mssql, p, ofp) < 0) {
				ListView_SetItemText(list, nitems, 1, "Failed");
				ListView_SetItemText(list, nitems, 2, get_last_error());
				allok = ok = false;
			}
			else
				ListView_SetItemText(list, nitems, 1, "Success");
			write_log(lf, "%15s exported\n", (ok ? "" : "not"));
			doprogress(win, PBC_METER);
			++nitems;
		}
	}
	if (dumpprocs) {			/* dump stored procs */
		bool ok = true;			/* dump proc was successful */
		fprintf(ofp, "\n-- Dumping procedures\n--\n");
		while ((p = get_proc_name(mssql)) != 0) {
			sprintf(temp, "Dumping stored procedure '%s'", p);
			addnewitem(list, nitems, item, temp);
			write_log(lf, ". exporting procedure %27s", p);
			if (dump_proc(mssql, p, ofp) < 0) {
				ListView_SetItemText(list, nitems, 1, "Failed");
				ListView_SetItemText(list, nitems, 2, get_last_error());
				allok = ok = false;
			}
			else
				ListView_SetItemText(list, nitems, 1, "Success");
			write_log(lf, "%15s exported\n", (ok ? "" : "not"));
			doprogress(win, PBC_METER);
			++nitems;
		}
	}
	/* we're done */
	LoadString(myapp, STR_STATUSLBL, temp, BUFSIZ);
	SetWindowText(win, temp);
	LoadString(myapp, STR_DISMISSBTN, temp, BUFSIZ);
	SetWindowText(GetDlgItem(win, BTN_CANCEL), temp);
	SendDlgItemMessage(win, PBC_METER, PBM_SETPOS,
			(int) SendDlgItemMessage(win,PBC_METER,PBM_GETRANGE,FALSE,0), 0);
	EnableWindow(GetDlgItem(win, BTN_VIEWLOG), TRUE);
	EnableWindow(GetDlgItem(win, BTN_OUTPUT), TRUE);
	done = true;
	fclose(ofp);				/* flush and close the output file */
	write_log(lf, "Export terminated %s\n",
			(allok ? "successfully without warnings." : "with warnings."));
	close_log(lf);
	return 0;
}
