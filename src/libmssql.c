/*
	libmssql - MySQL C-API like interface for Microsoft SQL Server
	
	Copyright (C) 2001-2002 Gopalarathnam V. <gopal@developercentral.org>.
	
	This library is free software; you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public
	License as published by the Free Software Foundation; either
	version 2.1 of the License, or (at your option) any later version.
	
	This library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	Lesser General Public License for more details.
	
	You should have received a copy of the GNU Lesser General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "mssql.h"

int last_errno;					/* last error number */
char last_error[BUFSIZ];		/* last error message */

int mssql_msg_handler(DBPROCESS* dbproc, DBINT msgno, int msgstate,
		int severity, const char* msgtext, const char* srvname,
		const char* procname, DBUSMALLINT line)
/* A custom message handler for DB-Library. */
{
	last_errno = msgno;
	strncpy(last_error, msgtext, BUFSIZ);
	UNREFERENCED_PARAMETER(dbproc);
	UNREFERENCED_PARAMETER(msgstate);
	UNREFERENCED_PARAMETER(severity);
	UNREFERENCED_PARAMETER(srvname);
	UNREFERENCED_PARAMETER(procname);
	UNREFERENCED_PARAMETER(line);
	return 0;
}

MSSQL* mssql_connect(char* host, char* user, char* passwd)
/*
	Connect to Microsoft SQL Server. On success, return a connection identifier
	that should be passed on subsequent calls to this library. If the
	connection could not be established, NULL is returned.
*/
{
	MSSQL* mssql;
	LOGINREC* login;
	DBPROCINFO dbpinfo;
	DWORD dwlen = MAX_COMPUTERNAME_LENGTH + 1;
	
	if ((mssql = (MSSQL*) malloc(sizeof(MSSQL))) == 0) return 0;
	/*
		If the host is NULL, try to determine the computer name of the local
		system and use it. If in case we could not determine the local computer
		name, we assume `localhost'.
	*/
	if (!host || !host[0]) {
		if ((host = (char*) malloc(dwlen)) == 0) return 0;
		if (!GetComputerName((LPTSTR) host, &dwlen))
			host = strdup("localhost");
	}
	mssql->host = strdup(host);
	/*
		If the user is NULL, the default login (sa) is used. If the passwd is
		NULL, blank password is used when connecting to Microsoft SQL Server.
	*/
	if (!user || !user[0]) user = strdup("sa");
	mssql->user = strdup(user);
	if (!passwd || !passwd[0]) passwd = strdup("");
	mssql->passwd = strdup(passwd);
	login = dblogin();			/* attempt to login to server */
	DBSETLUSER(login, user);
	DBSETLPWD(login, passwd);
	mssql->dbproc = dbopen(login, host);
	dbfreelogin(login);			/* job complete! free the login record */
	if (mssql->dbproc) {
		dbmsghandle(mssql_msg_handler);
		dbpinfo.SizeOfStruct = sizeof(DBPROCINFO);
		if (dbprocinfo(mssql->dbproc, &dbpinfo) == SUCCEED) {
			if ((mssql->server_version = (char*) malloc(BUFSIZ)) == 0)
				return 0;
			sprintf(mssql->server_version,
					"Microsoft SQL Server Version %d.%d.%d",
					dbpinfo.ServerMajor,
					dbpinfo.ServerMinor, dbpinfo.ServerRevision);
		}
	}
	return mssql;
}

void mssql_close(MSSQL* mssql)
/* Close connection with Microsoft SQL Server and free resources. */
{
	if (mssql) dbclose(mssql->dbproc);
}

int mssql_select_db(MSSQL* mssql, const char* db)
/*
	Set the current database. On success, 0 is returned. If the database could
	not be changed, a value of -1 is returned.
*/
{
	RETCODE rc;
  
	if ((rc = dbuse(mssql->dbproc, db)) == SUCCEED)
		mssql->db = strdup(db);
	return (rc == SUCCEED) ? 0 : -1;
}

int mssql_query(MSSQL* mssql, const char* q)
/*
	Execute a query.
	If the query was successful, the number of rows that would be returned in
	the result set is returned. Otherwise -1 is returned to indicate failure.
*/
{
	int i, nr_fields;
	DBCOL dbcol;				/* information about a column */
	MSSQL_FIELD* field;

	if (dbcmd(mssql->dbproc, q) == FAIL) return -1;
	if (dbsqlexec(mssql->dbproc) == FAIL) return -1;
	if (dbresults(mssql->dbproc) == FAIL) return -1;
	mssql->field_count = nr_fields = dbnumcols(mssql->dbproc);
	if (!nr_fields) return -1;	/* whoops! no columns returned? */
	mssql->affected_rows = dbcount(mssql->dbproc);
	if (!dbiscount(mssql->dbproc)) mssql->affected_rows = 0; /* almost! */
	mssql->fields = (MSSQL_FIELD *) malloc(nr_fields*sizeof(MSSQL_FIELD));
	if (!mssql->fields) return -1;
	for (i=0, field=mssql->fields; i < nr_fields; ++i, ++field) {
		dbcol.SizeOfStruct = sizeof(DBCOL);
		dbcolinfo((PDBHANDLE) mssql->dbproc, CI_REGULAR, i+1, 0, &dbcol);
		field->name = strdup(dbcol.Name);
		field->table = strdup(dbcol.TableName);
		field->def = 0;
		field->type = dbcol.Type;
		field->stype = (char*) dbprtype(dbcol.Type);
		field->maxlength = dbcol.MaxLength;
		field->precision = dbcol.Precision;
		field->scale = dbcol.Scale;
		field->null = dbcol.Null;
		field->identity = dbcol.Identity;
	}
	return mssql->affected_rows;
}

MSSQL_RES* mssql_list_dbs(MSSQL* mssql)
/*
	List all databases. A result set is returned that can be manipulated using
	mssql_fetch_row().
*/
{
	if (mssql_query(mssql, "EXECUTE sp_databases") < 0) return 0;
	return mssql_use_result(mssql);
}

MSSQL_RES* mssql_list_tables(MSSQL* mssql, const char* wild)
/*
	Return the list of tables in the current database as a result set. If wild
	is not NULL, only the list of tables matching wild is returned.
	The result set returned can be manipulated using mssql_fetch_row().
*/
{
	char sqlbuf[BUFSIZ];
	
	sprintf(sqlbuf, "EXECUTE sp_tables @table_name = \'%s%%\', "
			"@table_type = \"\'TABLE\'\"", (wild ? wild : ""));
	if (mssql_query(mssql, sqlbuf) < 0) return 0;
	return mssql_use_result(mssql);
}

MSSQL_RES* mssql_list_fields(MSSQL* mssql, const char* table, const char* wild)
/*
	Return the list of fields in the table `table' as a result set. If wild
	is not NULL, only the list of fields matching wild is returned.
	The result set returned can be manipulated using mssql_fetch_row().
*/
{
	char sqlbuf[BUFSIZ];
	
	sprintf(sqlbuf, "EXECUTE sp_columns @table_name = \'%s\', "
			"@column_name = \'%s%%\'", table, (wild ? wild : ""));
	if (mssql_query(mssql, sqlbuf) < 0) return 0;
	return mssql_use_result(mssql);
}

MSSQL_RES* mssql_list_indexes(MSSQL* mssql, const char* table)
/*
	Return the list of indexes for the table `table' as a result set. The
	result set returned can be manipulated using mssql_fetch_row().
*/
{
	char sqlbuf[BUFSIZ];
	
	sprintf(sqlbuf, "EXECUTE sp_helpindex @objname = \'%s\'", table);
	if (mssql_query(mssql, sqlbuf) < 0) return 0;
	return mssql_use_result(mssql);
}

MSSQL_RES* mssql_list_users(MSSQL* mssql)
/*
	Return the list of logins in the current database as a result set. The
	result set returned can be manipulated using mssql_fetch_row().
*/
{
	if (mssql_query(mssql, "EXECUTE sp_helpuser") < 0) return 0;
	return mssql_use_result(mssql);
}

MSSQL_RES* mssql_list_views(MSSQL* mssql, const char* wild)
/*
	Return the list of views in the current database as a result set. If wild
	is not NULL, only the list of users matching wild is returned.
	The result set returned can be manipulated using mssql_fetch_row().
*/
{
	char sqlbuf[BUFSIZ];
	
	sprintf(sqlbuf, "SELECT name FROM sysobjects "
			"WHERE (xtype = 'V') AND (name LIKE '%s%%') "
			"AND (OBJECTPROPERTY(id, 'IsMSShipped') = 0)", (wild ? wild : ""));
	if (mssql_query(mssql, sqlbuf) < 0) return 0;
	return mssql_use_result(mssql);
}

MSSQL_RES* mssql_list_procs(MSSQL* mssql, const char* wild)
/*
	Return the list of stored procedures in the current database as a result
	set. If wild is not NULL, only the list of procedures matching wild is
	returned.
	The result set returned can be manipulated using mssql_fetch_row().
*/
{
	char sqlbuf[BUFSIZ];
	
	sprintf(sqlbuf, "EXECUTE sp_stored_procedures @sp_name = \'%s%%\'",
			(wild ? wild : ""));
	if (mssql_query(mssql, sqlbuf) < 0) return 0;
	return mssql_use_result(mssql);
}

MSSQL_RES* mssql_list_udts(MSSQL* mssql)
/*
	Return the list of user-defined data types in the current database as a
	result set.
	The result set returned can be manipulated using mssql_fetch_row().
*/
{
	char sqlbuf[BUFSIZ];
	
	sprintf(sqlbuf, "SELECT st1.name, st2.name, "
			"st1.length, st1.allownulls, st1.prec, st1.scale "
			"FROM systypes st1, systypes st2 "
			"WHERE (st1.xtype = st2.xusertype) "
			"AND (st1.xtype != st1.xusertype)"); /* bad hack! */
	if (mssql_query(mssql, sqlbuf) < 0) return 0;
	return mssql_use_result(mssql);
}

MSSQL_RES* mssql_list_fkeys(MSSQL* mssql, const char* table)
/*
	Return the list of foreign keys in the table `table' as a result set. The
	result set returned can be manipulated using mssql_fetch_row().
*/
{
	char sqlbuf[BUFSIZ];
	
	sprintf(sqlbuf, "EXECUTE sp_fkeys \'%s\'", table);
	if (mssql_query(mssql, sqlbuf) < 0) return 0;
	return mssql_use_result(mssql);
}

short mssql_list_servers(char*** servers) /* pointer to pointer to pointer.. */
/* Return the list of registered Microsoft SQL Servers in the network. */
{
	char servlist[BUFSIZ];		/* null-terminated list of servers */
	unsigned short n;			/* number of servers */
	int i, rc;
	char** v;
	char* p = servlist;

	rc = dbserverenum(NET_SEARCH|LOC_SEARCH, servlist, BUFSIZ, &n);
	if (rc != ENUM_SUCCESS && rc != MORE_DATA)
		return -1;		/* error retrieving server list */
	v = (char**) malloc(n * sizeof(char*));
	for (i = 0; i < n; ++i) {
		int len;
		
		len = strlen(p) + 1;
		v[i] = (char*) malloc(len);	/* caller's responsibility to free! */
		strncpy(v[i], p, len);
		p += len;				/* jump to next entry */
	}
	*servers = v;				/* save server list and... */
	return n;					/* return total */
}

MSSQL_RES* mssql_use_result(MSSQL* mssql)
/*
	Allocate struct for use with unbuffered reads. The data is fetched when
	calling mssql_fetch_row().
*/
{
	int nr_fields;
	MSSQL_RES* result;
	MSSQL_FIELD* field;

	if ((result = (MSSQL_RES*) malloc(sizeof(MSSQL_RES))) == 0) return 0;
	result->row_count = mssql->affected_rows;
	result->field_count = nr_fields = mssql->field_count;
	result->fields = mssql->fields;
	result->handle = mssql;
	result->eof = 0;
	if ((result->lengths = (ulong*) malloc(nr_fields*sizeof(ulong))) == 0)
		return 0;
	else {
		int i;
		
		for (i=0, field=mssql->fields; i < nr_fields; ++i, ++field)
			result->lengths[i] = field->maxlength;
	}
	result->rowlen = 0;
	return result;				/* data is to be fetched */
}

int read_one_row(MSSQL_RES* res)
/* Read a single row. */
{
	uint fields, pos;
	MSSQL* mssql;
	MSSQL_ROW row;

	mssql = res->handle;
	fields = res->field_count;
	row = res->row;
	res->row[0] = 0;
	res->rowlen = 0;
	pos = 0;					/* pos where data is to be copied within row */
	if (dbnextrow(mssql->dbproc) != NO_MORE_ROWS) {
		uint fld;
		MSSQL_FIELD* pfld;
		
		for (fld=0, pfld=mssql->fields; fld < fields; ++fld, ++pfld) {
			uint fldlen;
			
			fldlen = dbdatlen(mssql->dbproc, fld+1);
			if (fldlen > 0) {
				char temp[SQLROWSIZ]; /* size of one row */
				uint len;		/* actual length of the field */
				
				dbconvert(mssql, pfld->type,
						(BYTE*) dbdata(mssql->dbproc, fld+1), fldlen,
						SQLCHAR, (BYTE*) temp, -1);
				len = strlen(temp);
				if (len != fldlen)
					fldlen = len; /* adjust collength */
				memcpy((row+pos), temp, fldlen+1);
				pos += fldlen+1;
			} else {			/* a null value column */
				row[pos] = 0;
				++pos;
			}
		}
	}
	else
		return -1;
	res->rowlen = pos - 1;
	return 0;
}

MSSQL_ROW mssql_fetch_row(MSSQL_RES* res)
/*
	Return next row of query results. While this function returns a complete
	row, columns can be extracted using mssql_fetch_col().
*/
{
	if (!res->eof) {
		if (read_one_row(res) < 0)
			res->eof = 1;
		else {
			++res->row_count;
			return res->row;
		}
	}
	return 0;
}

char* mssql_fetch_col(MSSQL_RES* res, uint col)
/* Return a single column as a null-terminated string. */
{
	char* p;
	uint i, j;

	j = 0;
	for (i = 0; j < col && i < res->rowlen; ++i)
		if (!res->row[i]) ++j;
	p = (res->row + i);
	return p;
}

void mssql_flush(MSSQL* mssql)
/* Cancel any pending command batch and flush pending records. */
{
	dbcancel(mssql->dbproc);
}
