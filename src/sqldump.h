/*
	Copyright (C) 2001-2002 Gopalarathnam V. <gopal@developercentral.org>.
	
	This file is a part of SqlDump.
	
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with SqlDump; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>
#include "mssql.h"
#include "config.h"

/* global constants and prototypes get defined here */
#ifndef _SQLDUMP_H
#define _SQLDUMP_H

/* getopt.c */
extern char* optarg;
extern int optind;
extern int opterr;
struct option {
	const char *name;
	int has_arg;
	int* flag;
	int val;
};

/* names for the values of the `has_arg' field of `struct option'. */
enum _argtype {
	no_argument,
	required_argument,
	optional_argument
};

extern int getopt(int argc, char* const* argv, const char* shortopts);
extern int getopt_long(int argc, char* const* argv, const char* shortopts,
		const struct option* longopts, int* longind);
extern int getopt_long_only(int argc, char* const* argv,
		const char* shortopts,
		const struct option* longopts, int* longind);

/* getpw.c */
extern char *getpw(const char *);

/* sqldump.c */
extern char* progname;
extern void usage_exit();
extern void version_info();

#endif /* whole file */
