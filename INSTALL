This is the file INSTALL for SqlDump.

The current version of this program is 3.0b1.

You can install SqlDump by grabbing the binary package which takes you step by
step and copies the program in your system.  Or, for the brave souls, you can
obtain the source package, configure it to suit your system and compile the
program yourself.  Both installation instructions are described below.

For program usage and known problems, see the file README.


INSTALLATION
************


Binary Installation
===================

If you have downloaded the pre-compiled binary package (sqldump-3.0b1-bin.exe),
you will need to run the program which will install SqlDump, and optionally the
sources for it.  The pre-compiled binaries have been tested under Windows
Millenium, Windows 95/98 and Windows NT/2000.  And it may fairly run on Windows
XP too.  My machine runs the plain old (vanilla) Windows 95, remember 4.00.950.
So, SqlDump now runs on Windows 95 too :-).

The installation should be fairly straightforward.  Once you start the setup
program, you will be shown the license terms for the usage and distribution of
this program (GPL).  If you agree to the terms, you proceed by clicking the
Next button.  The next screen will allow you to choose the components viz.,
base installation (required) and optionally the source code and program
shortcuts.  A complete installation will occupy no more than a Meg so don't
worry about disk space unless you are too short of it.  Proceeding you will be
allowed to choose the directory under which SqlDump will be installed.  By
default this may be something like`C:\Program Files\SqlDump', leave it as it
is if you haven't installed a previous version of SqlDump.  Thats all, the
setup program will copy the files to your system and create the program
shortcuts for you.

The program is no more than a few hundred kilobytes if you have performed a
complete install along with the sources.

The installation package is created using one of the best free installation
builders written by Jordan Russell -- Inno Setup, which is free for both
personal and commercial use.  It can be downloaded from:

	http://www.jrsoftware.org/

NOTE: If you have a previous version of SqlDump and want to keep that version,
you may need to choose a different directory during setup.  The installation
program will overwrite the binaries and sources without asking a question.


Source Installation
===================

Okay, you are one among the souls who would try to do things differently if not
the hard way.  You would have downloaded the source archive (something like
sqldump-3.0b1-src.tar.bz2), so you will need to unpack the file on to a
directory first.

Note: Starting with this release, SqlDump source package will come as a bzipped
tarball (tar.bz2).  To unpack the archive, you will need GNU tar and bzip2,
both of them freely on the net.

This program cleanly compiles with Microsoft Visual C++ 6.0 or higher, Borland
C++ 5.5 or higher, and now it supports MinGW compiler too.  So, you need to
have either of these C++ compilers on your system to build SqlDump.  I would
prefer MinGW (Minimalist GNU for Windows) or Borland C++ 5.5 since they are
freely available for download from the following location:

	http://www.borland.com/
	http://www.mingw.org/

Since, this release doesn't use the Wizard97 look and feel :-( it cleanly
compiles with Microsoft Visual C++ compiler 6.0 and upwards.

Once you have the requirements, building SqlDump is very easy.  Starting from
release 2.2, SqlDump comes with a configuration program that detects the
settings and automatically generates Makefiles to reduce the burden of fiddling
with the Makefiles.  So, all you have to do is open a command prompt
(command.com on Win9x or cmd.exe on WinNT), cd to the directory where you have
unpacked the sources, and run the configure program.

	C:\sqldump-3.0b1>configure --prefix=D:\sqldump

The configure program by itself accepts many parameters, try `configure --help'
for more information.  Again, be sure to set the path to your C++ compiler
before running the configure program or configure would complain that it can't
find any C++ compiler though you may have it installed.

The configure program attempts to detect various settings to help build SqlDump
and generates Makefiles under the root and src directories.  Once the
configuration is complete, you can compile SqlDump by issuing the command
`make' or `nmake' or `gmake' depending on which compiler you use to build
SqlDump.  The configure program will report that for you.

Note: To compile with MinGW compiler you need the gmake program.  If your MinGW
version comes with a make command named `make', copy it as `gmake' because
SqlDump identifies the MinGW compiler only by execing gmake.

	C:\sqldump-3.0b1>gmake all

Once the build is complete without any errors (obviously), issue the command
`gmake install' (or `nmake install') to install SqlDump under C:\SqlDump (by
default).  You may override the installation directory by using the `--prefix'
argument when configuring SqlDump.

	C:\sqldump-3.0b1>gmake install

If you have any problem compiling the sources, you can always write me at
<gopal@developercentral.org>.

You need to copy ntwdblib.dll manually to the Windows system (usually
C:\Windows\system for Win9x or C:\Windows\system32 for WinNT) directory before
running SqlDump if it is not already present in your system.

Thats all!
